# README #

Minus Infinity is a 3D game being built for Mac (Windows support is in planning too!). Currently, the technology to support the game is in development. The game is being written in C++, and uses OpenGL for rendering.

### How do I get set up? ###

Minus Infinity has only been build and tested on Mac. By using the makefile within the Source directory,the project can easily be built.

The project requires the following dependencies:

- SOIL2
- OpenGL
- GLEW
- GLFW
- GLM
- GLUT

### Who do I talk to? ###

You can email me at: mrsaturnsan@gmail.com

![Scheme](Assets/Screenshots/test1.png)
![Scheme](Assets/Screenshots/test2.png)