/******************************************************************************/
/*
* @file   light.cpp
* @author Aditya Harsh
* @brief  Handles light objects in the scene.
*/
/******************************************************************************/

#include "light.h"
#include "actor.h"

namespace mifty
{
    // static light objects
    namespace
    {
        Light* dir_light = nullptr;
    }

    /**
     * @brief Constructor for light objects
     * 
     * @param actor 
     * @param type 
     */
    Light::Light(Actor* actor, const glm::vec3& pos, const glm::vec3& color) : Component(actor), pos_(pos), color_(color)
    {
        // destroy an existing light object
        if (dir_light)
            dir_light->GetParent()->SetDestroy(true);
        // set pointer
        dir_light = this;
    }

    /**
     * @brief Destructor for light objects
     * 
     */
    Light::~Light()
    {
        dir_light = nullptr;
    }

    void Light::Update()
    {
        
    }

    void Light::AddPosition(const glm::vec3& pos) noexcept
    {
        pos_ += pos;
    }

    void Light::SetPosition(const glm::vec3& pos) noexcept
    {
        pos_ = pos;
    }

    const glm::vec3& Light::GetPosition() const noexcept
    {
        return pos_;
    }

    void Light::SetColor(const glm::vec3& col) noexcept
    {
        color_ = col;
    }

    const glm::vec3& Light::GetColor() const noexcept
    {
        return color_;
    }

    Light* Light::GetActiveLight() noexcept
    {
        return dir_light;
    }

    std::unique_ptr<Component> Light::Clone()
    {
        return std::make_unique<Light>(parent_, pos_, color_);
    }
}