/******************************************************************************/
/*
* @file   transform.cpp
* @author Aditya Harsh
* @brief  Transforms allow for movement, rotation, and scaling.
*/
/******************************************************************************/

#include "transform.h"
#include "actor.h"

namespace mifty
{
    /**
     * @brief Constructor for the transform component
     * 
     * @param parent 
     * @param position 
     * @param scale 
     * @param rotation 
     */
    Transform::Transform(Actor* parent, const glm::vec3& position, const glm::vec3& scale, const glm::vec3& rotation) : 
                         Component(parent), translation_(position), scale_(scale), rotation_(rotation), is_dirty_(true)
    {

    }

    /**
     * @brief Destructor for the transform component
     * 
     * @return Transform::~Transform 
     */
    Transform::~Transform() noexcept
    {
        
    }

    /**
     * @brief Directly changes the position of the transform
     * 
     * @param position 
     */
    void Transform::SetPosition(const glm::vec3& position) noexcept
    {
        translation_ = position;
        is_dirty_ = true;
    }

    /**
     * @brief Adds a position to the current position
     * 
     * @param position 
     */
    void Transform::AddPosition(const glm::vec3& position) noexcept
    {
        translation_ += position;
        is_dirty_ = true;
    }

    /**
     * @brief Gets the current position of the transform
     * 
     * @return const glm::vec3& Transform::GetPosition const 
     */
    const glm::vec3& Transform::GetPosition() const noexcept
    {
        return translation_;
    }

    /**
     * @brief Directly sets the scale
     * 
     * @param scale 
     */
    void Transform::SetScale(const glm::vec3& scale) noexcept
    {
        scale_ = scale;
        is_dirty_ = true;
    }

    /**
     * @brief Adds scale to the transform
     * 
     * @param scale 
     */
    void Transform::AddScale(const glm::vec3& scale) noexcept
    {
        scale_ += scale;
        is_dirty_ = true;
    }

    /**
     * @brief Gets the current scale of the transform component
     * 
     * @return const glm::vec3& Transform::GetScale const 
     */
    const glm::vec3& Transform::GetScale() const noexcept
    {
        return scale_;
    }

    /**
     * @brief Directly sets the rotation
     * 
     * @param rotation 
     */
    void Transform::SetRotation(const glm::vec3& rotation) noexcept
    {
        rotation_ = rotation;
        is_dirty_ = true;
    }

    /**
     * @brief Adds rotation
     * 
     * @param rotation 
     */
    void Transform::AddRotation(const glm::vec3& rotation) noexcept
    {
        rotation_ += rotation;
        is_dirty_ = true;
    }

    /**
     * @brief Gets the rotation
     * 
     * @return const glm::vec3& Transform::GetRotation const 
     */
    const glm::vec3& Transform::GetRotation() const noexcept
    {
        return rotation_;
    }

    /**
     * @brief Returns the main matrix stored inside the class
     * 
     * @return const glm::mat4& Transform::GetMatrix const 
     */
    const glm::mat4& Transform::GetMatrix() const noexcept
    {
        return matrix_;
    }

    /**
     * @brief Called every frame to update the transform component
     * 
     */
    void Transform::Update()
    {
        RecalculateMatrix();
    }

    /**
     * @brief Virtual copy constructor
     * 
     * @return std::unique_ptr<Component>
     */
    std::unique_ptr<Component> Transform::Clone()
    {
        return std::make_unique<Transform>(parent_, translation_, scale_, rotation_);
    }

    /**
     * @brief Recalculates the matrix whenever the flag is set
     * 
     */
    void Transform::RecalculateMatrix() noexcept
    {
        // check the flag to prevent redundant computations
        if (is_dirty_)
        {
            // reset to identity matrix
            matrix_ = glm::mat4();

            matrix_ = glm::translate(matrix_, translation_); // Translate 

            // rotate on all 3 axis
            matrix_ = glm::rotate(matrix_, rotation_.x, glm::vec3(1, 0, 0));
            matrix_ = glm::rotate(matrix_, rotation_.y, glm::vec3(0, 1, 0));
            matrix_ = glm::rotate(matrix_, rotation_.z, glm::vec3(0, 0, 1));

            matrix_ = glm::scale(matrix_, scale_); // Scale

            // reset flag
            is_dirty_ = false;
        }
    }

    /**
     * @brief Forces a matrix recalculation
     * 
     */
    void Transform::ForceRecalculate()
    {
        is_dirty_ = true;
        RecalculateMatrix();
    }
}