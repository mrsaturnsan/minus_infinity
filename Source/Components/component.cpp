/******************************************************************************/
/*
* @file   component.cpp
* @author Aditya Harsh
* @brief  Component abstract class.
*/
/******************************************************************************/

#include "component.h"
#include "actor.h"

namespace mifty
{
    /**
     * @brief Fills in component details
     * 
     * @param parent 
     */
    Component::Component(Actor* parent) noexcept : parent_(parent), active_(true)
    {
        assert(parent_ && "Valid parent pointer is required!");
    }

    /**
     * @brief Destructor for components (virtual)
     * 
     */
    Component::~Component()
    {
        
    }

    /**
     * @brief Checks whether or not the component is active
     * 
     * @return true 
     * @return false 
     */
    bool Component::Active() const noexcept
    {
        return active_;
    }

    /**
     * @brief Sets active/inactive
     * 
     * @param active 
     */
    void Component::SetActive(bool active) noexcept
    {
        active_ = active;
    }

    /**
     * @brief Gets the parent of the component
     * 
     * @return Actor const* GetParent 
     */
    Actor* Component::GetParent() noexcept
    {
        return parent_;
    }

    /**
     * @brief Changes the parent of a component
     * 
     * @param actor 
     */
    void Component::SetParent(Actor* actor)
    {
        if (actor)
        {
            parent_ = actor;
        }
        else
        {
            throw std::runtime_error("Invalid parent passed into Component::SetParent!");
        }
    }
}