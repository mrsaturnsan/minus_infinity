/******************************************************************************/
/*
* @file   playercontroller.cpp
* @author Aditya Harsh
* @brief  Controller handling player controls and movement
*/
/******************************************************************************/

#include "playercontroller.h"
#include "camera.h"
#include "gametime.h"
#include "input.h"
#include "physics.h"
#include "transform.h"

namespace mifty
{
    /**
     * @brief Constructor for the player's behavior
     * 
     * @param actor 
     * @param cam 
     */
    PlayerController::PlayerController(Actor* actor, Camera* cam, Physics* phy) : Component(actor), cam_(cam), phy_(phy)
    {
        if (!cam || !phy)
            throw std::runtime_error("Dependencies not satisfied");

        //phy->SetAcceleration(glm::vec3(0, -0.5f, 0));
    }

    /**
     * @brief Destructor
     * 
     * @return ~PlayerController 
     */
    PlayerController::~PlayerController()
    {
        
    }

    /**
     * @brief Update behavior
     * 
     */
    void PlayerController::Update()
    {
        float velocity = 10.0f * GameTime::GetDeltaTime();
        
        glm::vec3 pos;

        if (Input::InputCurrent(GLFW_KEY_W))
            pos += cam_->front_ * velocity;
        if (Input::InputCurrent(GLFW_KEY_A))
            pos -= cam_->right_ * velocity;
        if (Input::InputCurrent(GLFW_KEY_S))
            pos -= cam_->front_ * velocity;
        if (Input::InputCurrent(GLFW_KEY_D))
            pos += cam_->right_ * velocity;

        phy_->SetVelocity(pos);
        cam_->SetPosition(phy_->GetPosition());
    }

    /**
     * @brief Clone function
     * 
     * @return std::unique_ptr<Component> 
     */
    std::unique_ptr<Component> PlayerController::Clone()
    {
        return std::make_unique<PlayerController>(parent_, cam_, phy_);
    }

    /**
     * @brief Test function for block spawning
     * 
     */
    void PlayerController::SpawnBlock()
    {
        if (Input::InputTriggered(GLFW_MOUSE_BUTTON_LEFT))
        {
            // spawn a block
        }
    }
}