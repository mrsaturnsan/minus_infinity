/******************************************************************************/
/*
* @file   playercontroller.h
* @author Aditya Harsh
* @brief  Controller handling player controls and movement
*/
/******************************************************************************/

#pragma once

#include "component.h"

namespace mifty
{
    // forward declare
    class Actor;
    class Camera;
    class Physics;
    /**
     * @brief Handles player controls
     * 
     */
    class PlayerController : public Component
    {
    public:
        PlayerController(Actor* parent, Camera* cam, Physics* phy);
        virtual ~PlayerController() override;
        virtual void Update() override;
        virtual std::unique_ptr<Component> Clone() override;
    private:
        // pointer to the camera
        Camera* cam_;
        // physics pointer
        Physics* phy_;

        // how fast the player moves
        float move_speed_ = 50.0f;

        void SpawnBlock();
    };
}