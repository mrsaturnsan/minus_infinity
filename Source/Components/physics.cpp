/******************************************************************************/
/*
* @file   physics.cpp
* @author Aditya Harsh
* @brief  Resposible for anything physics related.
*/
/******************************************************************************/

#include "physics.h"
#include "actor.h"
#include "transform.h"
#include "boxcollider.h"
#include "gametime.h"
#include "jobmanager.h"

namespace mifty
{
    namespace
    {
        float CheckAxis(float min1, float max1, float min2, float max2)
        {
            return (min1 < min2) ? (min2 - max1) : (max2 - min1);
        }

        unsigned IndexSmallest(float array[], unsigned size)
        {
            unsigned index = 0;

            for (unsigned i = 1; i < size; ++i)
            {
                if(abs(array[i]) < abs(array[index]))
                    index = i;              
            }
            return index;
        }
    }
    /**
     * @brief Physics constructor
     * 
     * @param parent 
     * @param trs 
     */
    Physics::Physics(Actor* parent, Transform* trs, bool fixed) : Component(parent), trs_(trs), fixed_(fixed)
    {

    }

    /**
     * @brief Physics destructor
     * 
     */
    Physics::~Physics()
    {

    }
    
    /**
     * @brief Update function (called each frame)
     * 
     */
    void Physics::Update()
    {
        // account for acceleration
        velocity_ += acceleration_ * GameTime::GetDeltaTime();

        // account for velocity
        trs_->AddPosition(velocity_);
    }

    /**
     * @brief Virtual copy
     * 
     * @return std::unique_ptr<Component> 
     */
    std::unique_ptr<Component> Physics::Clone()
    {
        return std::make_unique<Physics>(parent_, trs_, fixed_);
    }

    /**
     * @brief Sets the velocity of the object
     * 
     * @param vel 
     */
    void Physics::SetVelocity(const glm::vec3& vel) noexcept
    {
        velocity_ = vel;
    }

    /**
     * @brief Adds velocity into the object
     * 
     * @param vel
     */
    void Physics::AddVelocity(const glm::vec3& vel) noexcept
    {
        velocity_ += vel;
    }

    /**
     * @brief Gets the velocity of the object
     * 
     * @return const glm::vec3& Physics::GetVelocity const 
     */
    const glm::vec3& Physics::GetVelocity() const noexcept
    {
        return velocity_;
    }

    /**
     * @brief Sets the acceleration of the object
     * 
     * @param acc
     */
    void Physics::SetAcceleration(const glm::vec3& acc) noexcept
    {
        acceleration_ = acc;
    }

    /**
     * @brief Adds acceleration into the object
     * 
     * @param acc 
     */
    void Physics::AddAcceleration(const glm::vec3& acc) noexcept
    {
        acceleration_ += acc;
    }

    /**
     * @brief Gets the acceleration of the object
     * 
     * @return const glm::vec3& Physics::GetAcceleration const 
     */
    const glm::vec3& Physics::GetAcceleration() const noexcept
    {
        return acceleration_;
    }

    /**
     * @brief Resolves collisions
     * 
     * @param other 
     */
    void Physics::ResolveCollision(BoxCollider* b1, BoxCollider* b2)
    {
        // return if either pointer is invalid or if fixed object
        if (fixed_ || !b1 || !b2) return;

        // vector for resolving the conflict
        glm::vec3 resolution;

        // array of distances
        float dist[] =  {
                            CheckAxis(b1->minx_, b1->maxx_, b2->minx_, b2->maxx_),
                            CheckAxis(b1->miny_, b1->maxy_, b2->miny_, b2->maxy_),
                            CheckAxis(b1->minz_, b1->maxz_, b2->minz_, b2->maxz_)
                        };

        // find out the axis of least separation
        unsigned index = IndexSmallest(dist, 3);

        // how much penetration occured
        float pen = dist[index];

        glm::vec3 new_vec = (velocity_ * (mass_ - b2->phy_->mass_) + (2.0f * b2->phy_->mass_ * b2->phy_->velocity_)) / (mass_ + b2->phy_->mass_);

        switch(index)
        {
        case 0:
            velocity_.x = new_vec.x;
            resolution.x = dist[0];
            break;
        case 1:
            velocity_.y = new_vec.y;
            resolution.y = dist[1];
            break;
        case 2:
            velocity_.z = new_vec.z;
            resolution.z = dist[2];
            break;
        default:
            throw std::runtime_error("Something went wrong in collision resolution!");
        }

        const float percent = 0.3f; // usually 20% to 80%
        const float slop = 0.01f; // usually 0.01 to 0.1
        glm::vec3 correction = std::max(pen - slop, 0.0f ) / 2.0f * percent * resolution;
        trs_->AddPosition(correction);
        trs_->AddPosition(resolution);
    }


    /**
     * @brief Gets the position of the object
     * 
     * @return const glm::vec3& Physics::GetPosition const 
     */
    const glm::vec3& Physics::GetPosition() const noexcept
    {
        return trs_->GetPosition();
    }

    /**
     * @brief Gets the rotation
     * 
     * @return const glm::vec3& Physics::GetRotation const 
     */
    const glm::vec3& Physics::GetRotation() const noexcept
    {
        return trs_->GetRotation();
    }
}