/******************************************************************************/
/*
* @file   boxcollider.cpp
* @author Aditya Harsh
* @brief  Handles collisions (AABB)
*/
/******************************************************************************/

#include "boxcollider.h"
#include "transform.h"
#include "physics.h"
#include "utility.h"
#include "actor.h"
#include "collisionmanager.h"
#include "jobmanager.h"

namespace mifty
{
    /**
     * @brief Constructor for box collider
     * 
     * @param parent 
     * @param phy 
     * @param bounds 
     */
    BoxCollider::BoxCollider(Actor* parent, Physics* phy, const glm::vec3& bounds, bool fixed) : Component(parent), phy_(phy), bounds_(bounds), fixed_(fixed)
    {
        // register self for collision detection
        CalculateBounds();
        CollisionManager::Register(this);
    }

    /**
     * @brief Box collider destructor
     * 
     */
    BoxCollider::~BoxCollider()
    {
        
    }

    /**
     * @brief Update function called each frame
     * 
     */
    void BoxCollider::Update()
    {
        if (!fixed_)
            CalculateBounds();
    }

    /**
     * @brief Creates a copy of the box collider
     * 
     * @return std::unique_ptr<Component> 
     */
    std::unique_ptr<Component> BoxCollider::Clone()
    {
        return std::make_unique<BoxCollider>(parent_, phy_, bounds_, fixed_);
    }

    /**
     * @brief Checks collision with another box collider
     * 
     * @param other 
     * @return true 
     * @return false 
     */
    bool BoxCollider::CheckCollision(const BoxCollider* other)
    {
        if (!other) throw std::runtime_error("Invalid collider");

        // perform AABB check
        return (this != other) &&
               (minx_ <= other->maxx_ && maxx_ >= other->minx_) && 
               (miny_ <= other->maxy_ && maxy_ >= other->miny_) && 
               (minz_ <= other->maxz_ && maxz_ >= other->minz_);
    }

    /**
     * @brief Resizes the box collider
     * 
     * @param bounds 
     */
    void BoxCollider::Resize(const glm::vec3& bounds)
    {
        bounds_ = bounds;
    }

    /**
     * @brief Gets the physics pointer attached to the collider
     * 
     * @return Physics* 
     */
    Physics* BoxCollider::GetPhysics()
    {
        return phy_;
    }

    /**
     * @brief Calculates the bounds
     * 
     */
    void BoxCollider::CalculateBounds()
    {
        const glm::vec3& pos = phy_->GetPosition();
        const glm::vec3& rot = phy_->GetRotation();

        // 8 points on a box
        std::array<glm::vec3, 8> points
        {
            glm::vec3(bounds_.x, bounds_.y, -bounds_.z),
            glm::vec3(-bounds_.x, bounds_.y, -bounds_.z),
            glm::vec3(-bounds_.x, bounds_.y, bounds_.z),
            glm::vec3(bounds_.x, bounds_.y, bounds_.z),
            glm::vec3(bounds_.x, -bounds_.y, -bounds_.z),
            glm::vec3(-bounds_.x, -bounds_.y, -bounds_.z),
            glm::vec3(-bounds_.x, -bounds_.y, bounds_.z),
            glm::vec3(bounds_.x, -bounds_.y, bounds_.z)
        };

        // create a rotation matrix
        glm::mat4 rot_mat;
        rot_mat = glm::rotate(rot_mat, rot.x, glm::vec3(1, 0, 0));
        rot_mat = glm::rotate(rot_mat, rot.y, glm::vec3(0, 1, 0));
        rot_mat = glm::rotate(rot_mat, rot.z, glm::vec3(0, 0, 1));
        
        // move each point to the appropriate location
        for (unsigned i = 0; i < 8; ++i)
        {
            points[i] = glm::vec3(rot_mat * glm::vec4(points[i], 1.0f));
            points[i] += pos;
        }

        // calculate minimum and maximum points
        minx_ = std::min_element(std::cbegin(points), std::cend(points), [](const auto& a, const auto& b){return a.x < b.x;})->x;
        maxx_ = std::max_element(std::cbegin(points), std::cend(points), [](const auto& a, const auto& b){return a.x < b.x;})->x;
        miny_ = std::min_element(std::cbegin(points), std::cend(points), [](const auto& a, const auto& b){return a.y < b.y;})->y;
        maxy_ = std::max_element(std::cbegin(points), std::cend(points), [](const auto& a, const auto& b){return a.y < b.y;})->y;
        minz_ = std::min_element(std::cbegin(points), std::cend(points), [](const auto& a, const auto& b){return a.z < b.z;})->z;
        maxz_ = std::max_element(std::cbegin(points), std::cend(points), [](const auto& a, const auto& b){return a.z < b.z;})->z;
    }
}