/******************************************************************************/
/*
* @file   physics.h
* @author Aditya Harsh
* @brief  Resposible for anything physics related.
*/
/******************************************************************************/

#pragma once

#include "component.h"

namespace mifty
{
    // forward declare
    class Actor;
    class Transform;
    class BoxCollider;

    /**
     * @brief Physics component takes care of collision resolution, gravity, etc
     * 
     */
    class Physics : public Component
    {
    public:
        // constructor
        Physics(Actor* parent, Transform* trs, bool fixed);
        // virtual destructor
        virtual ~Physics() override;
        // all deriving classes must hold an update function
        virtual void Update() override;
        // virtual copy call
        virtual std::unique_ptr<Component> Clone() override;
        // sets velocity
        void SetVelocity(const glm::vec3& vel) noexcept;
        // adds velocity
        void AddVelocity(const glm::vec3& vel) noexcept;
        // gets velocity
        const glm::vec3& GetVelocity() const noexcept;
        // sets acceleration
        void SetAcceleration(const glm::vec3& acc) noexcept;
        // adds acceleration
        void AddAcceleration(const glm::vec3& acc) noexcept;
        // gets acceleration
        const glm::vec3& GetAcceleration() const noexcept;
        // called whenever a colision has occured
        void ResolveCollision(BoxCollider* b1, BoxCollider* b2);
        // gets the position
        const glm::vec3& GetPosition() const noexcept;
        // gets the rotation
        const glm::vec3& GetRotation() const noexcept;
    private:
        // pointer to the transform component
        Transform* trs_;

        // whether or not the object can move from collision resolution
        bool fixed_;
        
        // physics
        float mass_ = 1.0f;
        float restitution_ = 0.0f;
        float friction_;
        // linear
        glm::vec3 velocity_;
        glm::vec3 acceleration_;
        // rotational
        glm::vec3 r_velocity_;
        glm::vec3 r_acceleration_;
    };
}