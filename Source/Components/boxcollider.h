/******************************************************************************/
/*
* @file   boxcollider.h
* @author Aditya Harsh
* @brief  Handles collisions (AABB)
*/
/******************************************************************************/

#pragma once

#include "component.h"

namespace mifty
{
    // forward declare
    class Actor;
    class Physics;

    /**
     * @brief Box collider implementation (AABB)
     * 
     */
    class BoxCollider : public Component
    {
        // friend physics class
        friend class Physics;
        friend class OctTree;
    public:
        // constructor
        BoxCollider(Actor* parent, Physics* phy, const glm::vec3& bounds, bool fixed);
        // virtual destructor
        virtual ~BoxCollider() override;
        // all deriving classes must hold an update function
        virtual void Update() override;
        // virtual copy
        virtual std::unique_ptr<Component> Clone() override;
        // checks collision
        bool CheckCollision(const BoxCollider* other);
        // resizes the box collider
        void Resize(const glm::vec3& bounds);
        // gets the physics pointer
        Physics* GetPhysics();
    private:
        // calculates the bounds
        void CalculateBounds();
        // the physics component attached to the object
        Physics* phy_;
        // how big is the collider
        glm::vec3 bounds_;
        // whether or not the object stays in place
        bool fixed_;

        // min and max
        float minx_ = 0;
        float maxx_ = 0;
        float miny_ = 0;
        float maxy_ = 0;
        float minz_ = 0;
        float maxz_ = 0;
    };
}