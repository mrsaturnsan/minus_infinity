/******************************************************************************/
/*
* @file   render.h
* @author Aditya Harsh
* @brief  Sets up any necessary data to be sent to the GPU.
*/
/******************************************************************************/

#pragma once

#include "component.h"
#include "meshmanager.h"
#include "material.h"
#include "texturemanager.h"

namespace mifty
{
    // forward declare
    class Transform;

    class Render : public Component
    {
    public:
        // constructor
        Render(Actor* parent, MeshID ID, TextureType texture, Material& mat);
        // pure virtual
        virtual void Update() override;
        // pure virtual
        virtual std::unique_ptr<Component> Clone() override;
        // get the ID of the mesh currently being used
        MeshID GetMesh() const noexcept;
        // gets the texture of the mesh
        TextureType GetTexture() const noexcept;
    private:
        // the ID of the mesh
        MeshID ID_;
        // the texture being used
        TextureType texture_;
        // which material the renderer is hooked up to
        Material& mat_;
    };
}