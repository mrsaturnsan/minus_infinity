/******************************************************************************/
/*
* @file   component.h
* @author Aditya Harsh
* @brief  Component abstract class.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    // forward declare
    class Actor;

    /**
     * @brief Component abstract class
     * 
     */
    class Component
    {
        friend class Actor;
    public:
        // component constructor (note: requires a pointer to the parent)
        Component(Actor* parent) noexcept;

        // virtual destructor
        virtual ~Component();
        // all deriving classes must hold an update function
        virtual void Update() = 0;
        // virtual copy
        virtual std::unique_ptr<Component> Clone() = 0;

        // gets the active state
        bool Active() const noexcept;
        // sets the active state
        void SetActive(bool active) noexcept;

        // returns the parent of the actor
        Actor* GetParent() noexcept;

    protected:
        // ptr to the parent
        Actor* parent_;

    private:
        // allows the parent of a component to be changed
        void SetParent(Actor* actor);
        // active or deactive
        bool active_;
    };
}
