/******************************************************************************/
/*
* @file   render.cpp
* @author Aditya Harsh
* @brief  Sets up any necessary data to be sent to the GPU.
*/
/******************************************************************************/

#include "render.h"
#include "actor.h"
#include "mat_vertex_color.h"
#include "jobmanager.h"

namespace mifty
{
    /**
     * @brief Constructor
     * 
     * @param parent 
     */
    Render::Render(Actor* parent, MeshID ID, TextureType texture, Material& mat) : Component(parent), ID_(ID), texture_(texture), mat_(mat)
    {

    }

    /**
     * @brief Update function
     * 
     */
    void Render::Update()
    {
        mat_.Update(*this);
    }

    /**
     * @brief Clone function
     * 
     * @return std::unique_ptr<Component> 
     */
    std::unique_ptr<Component> Render::Clone()
    {
        return std::make_unique<Render>(parent_, ID_, texture_, mat_);
    }

    /**
     * @brief Gets the mesh the renderer is currently using
     * 
     * @return MeshID Render::GetMesh 
     */
    MeshID Render::GetMesh() const noexcept
    {
        return ID_;
    }

    /**
     * @brief Gets the current texture being used.
     * 
     * @return TextureType Render::GetTexture const 
     */
    TextureType Render::GetTexture() const noexcept
    {
        return texture_;
    }
}