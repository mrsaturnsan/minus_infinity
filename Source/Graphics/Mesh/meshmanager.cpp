/******************************************************************************/
/*
* @file   meshmanager.cpp
* @author Aditya Harsh
* @brief  Manages the meshes that exist within memory.
*/
/******************************************************************************/

#include "meshmanager.h"

namespace mifty
{
    // private namespace
    namespace
    {
        // mesh with VAO, VBO, and EBO
        std::map<MeshID, std::tuple<GLuint, GLuint, GLuint>> mesh;
    }

    /**
     * @brief Loads a specified mesh
     * 
     * @param id 
     */
    void MeshManager::LoadMesh(MeshID id)
    {
        if (mesh.find(id) == mesh.end())
        {
            switch (id)
            {
            case MeshID::CUBE:
                LoadCube();
                break;
            case MeshID::PLANE:
                LoadPlane();
                break;
            }
        }
    }

    /**
     * @brief Removes a specified mesh
     * 
     * @param id 
     */
    void MeshManager::RemoveMesh(MeshID id)
    {
        if (mesh.find(id) != mesh.end())
        {
            // get the data
            auto& temp_tuple = mesh[id];

            // delete used data
            glDeleteVertexArrays(1, &std::get<0>(temp_tuple));
            glDeleteBuffers(1, &std::get<1>(temp_tuple));
            glDeleteBuffers(1, &std::get<2>(temp_tuple));

            // erase the component
            mesh.erase(id);
        }
    }

    /**
     * @brief Removes all meshes from memory
     * 
     */
    void MeshManager::RemoveAllMeshes()
    {
        for (auto& i : mesh)
        {
            auto& temp_tuple = i.second;
            glDeleteVertexArrays(1, &std::get<0>(temp_tuple));
            glDeleteBuffers(1, &std::get<1>(temp_tuple));
            glDeleteBuffers(1, &std::get<2>(temp_tuple));
        }
        mesh.clear();
    }

    /**
     * @brief Gets the data for the object
     * 
     * @param id 
     * @return const std::tuple<GLuint, GLuint, GLuint>& 
     */
    const std::tuple<GLuint, GLuint, GLuint>& MeshManager::GetMesh(MeshID id)
    {
        if (mesh.find(id) != mesh.end())
            return mesh[id];
            
        // should not have gone here
        throw std::runtime_error("Invalid enum!");
    }

    /**
     * @brief Creates the cube mesh and stores it in memory
     * 
     */
    void MeshManager::LoadCube()
    {
        // buffer objects
        GLuint VAO, VBO, EBO;

        GLfloat vertices[] = 
        {
            // positions          // texture Coords

            // front
            -1.0, -1.0,  1.0,     1.0, 1.0,
            1.0, -1.0,  1.0,      0.0, 1.0,
            
            1.0,  1.0,  1.0,      0.0, 0.0,
            -1.0,  1.0,  1.0,     1.0, 0.0,

            // top
            1.0, 1.0,  -1.0,       1.0, 1.0,
            -1.0,  1.0,  -1.0,     0.0, 1.0,
            -1.0,  1.0,  1.0,      0.0, 0.0,
            1.0, 1.0,  1.0,        1.0, 0.0,

            // back
            1.0, -1.0,  -1.0,     1.0, 1.0,
            -1.0, -1.0,  -1.0,      0.0, 1.0,
            -1.0,  1.0,  -1.0,      0.0, 0.0,
            1.0,  1.0,  -1.0,     1.0, 0.0,

            // bottom
            -1.0, -1.0,  -1.0,     1.0, 1.0,
            1.0,  -1.0,  -1.0,     0.0, 1.0,
            1.0,  -1.0,  1.0,      0.0, 0.0,
            -1.0, -1.0,  1.0,      1.0, 0.0,

            // left
            -1.0, -1.0,  -1.0,     1.0, 1.0,
            -1.0, -1.0,  1.0,      0.0, 1.0,
            -1.0,  1.0,  1.0,      0.0, 0.0,
            -1.0,  1.0,  -1.0,     1.0, 0.0,

            // right
            1.0, 1.0,  -1.0,     0.0, 0.0,
            1.0, 1.0,  1.0,      1.0, 0.0,
            1.0, -1.0,  1.0,     1.0, 1.0,
            1.0,  -1.0,  -1.0,   0.0, 1.0


        };

        // array for element buffer
        GLushort indices[] = 
        {
            // front
            0, 1, 2,
            2, 3, 0,
            // top
            4, 5, 6,
            6, 7, 4,
            // back
            8, 9, 10,
            10, 11, 8,
            // bottom
            12, 13, 14,
            14, 15, 12,
            // left
            16, 17, 18,
            18, 19, 16,
            // right
            20, 21, 22,
            22, 23, 20,
        };

        // generates buffers in one go
        GenerateBuffers(VAO, VBO, EBO, vertices, sizeof(vertices), indices, sizeof(indices));     

        // add to the map
        mesh[MeshID::CUBE] = std::make_tuple(VAO, VBO, EBO);
    }

    void MeshManager::LoadPlane()
    {
        // buffer objects
        GLuint VAO, VBO, EBO;

        // array of vertices for each part of the cube
        GLfloat vertices[] = 
        {
            1.0f, 1.0f, -1.0f,      1.0f, 1.0f, // top right
            -1.0f, 1.0f, -1.0f,     0.0, 1.0f, // top left
            -1.0f, 1.0f, 1.0f,      0.0, 0.0,  // bottom left
            1.0f, 1.0f, 1.0f,       1.0f, 0.0 // bottom right
        };

        // array for element buffer
        GLushort indices[] = 
        {
            0, 1, 2,  // first tri
            0, 2, 3  // second tri
        };

        // generates buffers in one go
        GenerateBuffers(VAO, VBO, EBO, vertices, sizeof(vertices), indices, sizeof(indices));     

        // add to the map
        mesh[MeshID::PLANE] = std::make_tuple(VAO, VBO, EBO);
    }

    /**
     * @brief Helper function for loading a mesh
     * 
     * @param VAO 
     * @param VBO 
     * @param EBO 
     * @param vertices 
     * @param size_vert 
     * @param indices 
     * @param size_ind 
     */
    void MeshManager::GenerateBuffers(GLuint& VAO, GLuint& VBO, GLuint& EBO, GLfloat* vertices, unsigned size_vert, GLushort* indices, unsigned size_ind)
    {
        // generate the buffers
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glGenBuffers(1, &EBO);

        // bind the VAO
        glBindVertexArray(VAO);

        // bind the VBO
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, size_vert, vertices, GL_STATIC_DRAW);

        // bind the EBO
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, size_ind, indices, GL_STATIC_DRAW);

        // enable attributes
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);

        // normal attrib
        glEnableVertexAttribArray(5);
        glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

        // texture attrib
        glEnableVertexAttribArray(6);
        glVertexAttribPointer(6, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

        // unbind the VBO
        glBindBuffer(GL_ARRAY_BUFFER, 0); 

        // unbind the VAO
        glBindVertexArray(0);
    }
}