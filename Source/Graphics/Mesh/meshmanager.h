/******************************************************************************/
/*
* @file   meshmanager.h
* @author Aditya Harsh
* @brief  Manages the meshes that exist within memory.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    // enum classes representing the meshes
    enum class MeshID
    {
        CUBE,
        PLANE
    };
    class MeshManager
    {
    public:
        // loads a mesh into memory
        static void LoadMesh(MeshID id);
        // removes given meshe from memory
        static void RemoveMesh(MeshID id);
        // removes all meshes from memory
        static void RemoveAllMeshes();
        // gets a EBO
        static const std::tuple<GLuint, GLuint, GLuint>& GetMesh(MeshID id);
    private:
        // disable instantiations of the class
        MeshManager() = delete;

        // functions for creating meshes
        static void LoadCube();
        static void LoadPlane();
        
        // helper function for generating buffers
        static void GenerateBuffers(GLuint& VAO, GLuint& VBO, GLuint& EBO, GLfloat* vertices, unsigned size_vert, GLushort* indices, unsigned size_ind);
    };
}