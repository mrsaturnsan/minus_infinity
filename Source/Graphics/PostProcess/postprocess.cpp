/******************************************************************************/
/*
* @file   postprocess.cpp
* @author Aditya Harsh
* @brief  Postprocessing class
*/
/******************************************************************************/

#include "postprocess.h"
#include "mat_post_process.h"
#include "graphics.h"
#include "gametime.h"

namespace mifty
{
    // static variables
    namespace
    {
        // vertex buffer object for the render texture
        GLuint quadVAO;
        GLuint quadVBO;

        // frame buffer object
        GLuint FBO;
        // render buffer
        GLuint RBO;
        // texture
        GLuint texture;
        // material
        std::unique_ptr<MatPostProcess> PostPro;
    }

    /**
     * @brief Initializes the post processor
     * 
     */
    void PostProcess::Init(const glm::i32vec2& dim)
    {
        // initialize the shader
        PostPro = std::make_unique<MatPostProcess>();

        // generate the quad
        GenQuad();

        // create the new frame buffer
        glGenFramebuffers(1, &FBO);
        glBindFramebuffer(GL_FRAMEBUFFER, FBO);

        GenTexture(dim.x, dim.y);

        GenRenderBuffer(dim.x, dim.y);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    /**
     * @brief Handles resizing of the screen
     * 
     * @param dim 
     */
    void PostProcess::Resize(const glm::i32vec2& dim)
    {
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, dim.x, dim.y, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glBindRenderbuffer(GL_RENDERBUFFER, RBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, dim.x, dim.y);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
    }

    /**
     * @brief Generates a texture
     * 
     */
    void PostProcess::GenTexture(GLuint x, GLuint y)
    {
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, x, y, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
    }

    /**
     * @brief Generates the render buffer
     * 
     * @param x 
     * @param y 
     */
    void PostProcess::GenRenderBuffer(GLuint x, GLuint y)
    {
        glGenRenderbuffers(1, &RBO);
        glBindRenderbuffer(GL_RENDERBUFFER, RBO); 
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, x, y);  
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n";
    }

    void PostProcess::GenQuad()
    {
        float quad_vert[] = 
        {
            // positions   // texCoords
            -1.0f,  1.0f,  0.0f, 1.0f,
            -1.0f, -1.0f,  0.0f, 0.0f,
            1.0f, -1.0f,   1.0f, 0.0f,

            -1.0f,  1.0f,  0.0f, 1.0f,
            1.0f, -1.0f,   1.0f, 0.0f,
            1.0f,  1.0f,   1.0f, 1.0f
        };

        // screen quad VAO
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quad_vert), &quad_vert, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
    }

    /**
     * @brief Begins drawing to the frame buffer
     * 
     */
    void PostProcess::Begin()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, FBO);

        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            throw std::runtime_error("Failed to bind the framebuffer");

        glEnable(GL_DEPTH_TEST);
    }

    /**
     * @brief Sends data to the GPU
     * 
     */
    void PostProcess::Draw()
    {
        // defer to the defaut framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // draw flat
        glDisable(GL_DEPTH_TEST);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // enable the shader
        PostPro.get()->Draw();

        // draw the quad
        glBindVertexArray(quadVAO);
        glBindTexture(GL_TEXTURE_2D, texture);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

    /**
     * @brief Updates any effects (debugging purposes for now)
     * 
     */
    void PostProcess::Update()
    {
        PostPro.get()->GetShader().SetFloat("deltaTime", GameTime::GetDeltaTime());
        PostPro.get()->GetShader().SetFloat("totalTime", GameTime::GetTimeSinceStart());
    }

    /**
     * @brief Frees allocated memory
     * 
     */
    void PostProcess::ShutDown()
    {
        glDeleteFramebuffers(1, &FBO);
        glDeleteVertexArrays(1, &quadVAO);
        glDeleteBuffers(1, &quadVBO);
    }
}