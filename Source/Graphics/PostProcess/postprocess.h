/******************************************************************************/
/*
* @file   postprocess.h
* @author Aditya Harsh
* @brief  Postprocessing class
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    class PostProcess
    {
    public:
        static void Init(const glm::i32vec2& dim);
        static void Resize(const glm::i32vec2& dim);
        static void Update();
        static void Begin();
        static void Draw();
        static void ShutDown();
    private:
        // static class
        PostProcess() = delete;

        static void GenTexture(GLuint x, GLuint y);
        static void GenRenderBuffer(GLuint x, GLuint y);
        static void GenQuad();
    };
}