/******************************************************************************/
/*
* @file   texturemanager.h
* @author Aditya Harsh
* @brief  Texture object
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    class Texture2D
    {
        friend class TextureManager;
    public:
        // Constructor (sets default texture modes)
        Texture2D();
        // Generates texture from image data
        void Generate(GLuint width, GLuint height, unsigned char* data);
        // Binds the texture as the current active GL_TEXTURE_2D texture object
        void Bind() const;
    private:
        // Holds the ID of the texture object, used for all texture operations to reference to this particlar texture
        GLuint ID_;
        // Texture image dimensions
        GLuint Width_, Height_; // Width and height of loaded image in pixels
        // Texture Format
        GLuint Internal_Format_; // Format of texture object
        GLuint Image_Format_; // Format of loaded image
        // Texture configuration
        GLuint Wrap_S_; // Wrapping mode on S axis
        GLuint Wrap_T_; // Wrapping mode on T axis
        GLuint Filter_Min_; // Filtering mode if texture pixels < screen pixels
        GLuint Filter_Max_; // Filtering mode if texture pixels > screen pixels
    };
}