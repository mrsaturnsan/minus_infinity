/******************************************************************************/
/*
* @file   texture.cpp
* @author Aditya Harsh
* @brief  Texture object
*/
/******************************************************************************/

#include "texture.h"

namespace mifty
{
    Texture2D::Texture2D() : Width_(0), Height_(0), Internal_Format_(GL_RGB), Image_Format_(GL_RGB), Wrap_S_(GL_REPEAT), Wrap_T_(GL_REPEAT), Filter_Min_(GL_NEAREST), Filter_Max_(GL_NEAREST)
    {
        glGenTextures(1, &ID_);
    }

    void Texture2D::Generate(GLuint width, GLuint height, unsigned char* data)
    {
        Width_ = width;
        Height_ = height;

        // Create Texture
        glBindTexture(GL_TEXTURE_2D, ID_);
        glTexImage2D(GL_TEXTURE_2D, 0, Internal_Format_, width, height, 0, Image_Format_, GL_UNSIGNED_BYTE, data);

        // Set Texture wrap and filter modes
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, Wrap_S_);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, Wrap_T_);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, Filter_Min_);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, Filter_Max_);

        // Unbind texture
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void Texture2D::Bind() const
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, ID_);
    }
}