/******************************************************************************/
/*
* @file   texturemanager.cpp
* @author Aditya Harsh
* @brief  Manages the textures that exist within memory.
*/
/******************************************************************************/

#include "texturemanager.h"

namespace mifty
{
    namespace
    {
        std::unordered_map<TextureType, Texture2D> textures;
    }

    /**
     * @brief Loads a texture
     * 
     * @param id 
     * @param alpha 
     */
    void TextureManager::LoadTexture(const char* file, TextureType id, bool alpha)
    {
        textures[id] = LoadFromFile(file, alpha);
    }

    /**
     * @brief Removes a specified texture
     * 
     * @param id 
     */
    void TextureManager::RemoveTexture(TextureType id)
    {
        textures.erase(id);
    }

    /**
     * @brief Removes all textures
     * 
     */
    void TextureManager::RemoveAllTextures()
    {
        textures.clear();
    }

    /**
     * @brief Returns the texture
     * 
     * @param id 
     * @return Texture2D 
     */
    Texture2D& TextureManager::GetTexture(TextureType id)
    {
        return textures[id];
    }

    /**
     * @brief Loads a texture from a file
     * 
     * @param file 
     * @param alpha 
     * @return Texture2D 
     */
    Texture2D TextureManager::LoadFromFile(const char* file, bool alpha)
    {
        // Create Texture object
        Texture2D texture;

        if (alpha)
        {
            texture.Internal_Format_ = GL_RGBA;
            texture.Image_Format_ = GL_RGBA;
        }

        // Load image
        int width, height;
        unsigned char* image = SOIL_load_image(file, &width, &height, 0, texture.Image_Format_ == GL_RGBA ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);

        // Now generate texture
        texture.Generate(width, height, image);

        // And finally free image data
        SOIL_free_image_data(image);
        return texture;
    }
}