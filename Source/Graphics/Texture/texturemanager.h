/******************************************************************************/
/*
* @file   texturemanager.h
* @author Aditya Harsh
* @brief  Manages the textures that exist within memory.
*/
/******************************************************************************/

#pragma once

#include "texture.h"

namespace mifty
{
    /**
     * @brief The type of texture.
     * 
     */
    enum class TextureType
    {
        NONE,
        GRASS,
        DIRT,
        SAND,
        COBBLE,
        BLOCK,
        GREEN
    };

    class TextureManager
    {
    public:
        // loads a mesh into memory
        static void LoadTexture(const char* file, TextureType id, bool alpha = true);
        // removes given meshe from memory
        static void RemoveTexture(TextureType id);
        // removes all meshes from memory
        static void RemoveAllTextures();
        // gets the texture
        static Texture2D& GetTexture(TextureType id);
    private:
        // static class
        TextureManager() = delete;   
        static Texture2D LoadFromFile(const char* file, bool alpha);
    };
}