/******************************************************************************/
/*
* @file   shader.h
* @author Aditya Harsh
* @brief  Resposible for managing shaders.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    /**
     * @brief General purpose shader class
     * 
     */
    class Shader
    {
    public:
        // Constructor
        Shader() = default;
        // Compiles a shader
        void Compile(const GLchar* vertexSource, const GLchar* fragmentSource, const GLchar* geometrySource = nullptr);
        // Sets the current shader as active
        void Use();
        // Utility functions
        // Sets a float in the shader
        void SetFloat (const GLchar* name, GLfloat value, GLboolean use_shader = false);
        // Sets an integer in the shader
        void SetInteger (const GLchar* name, GLint value, GLboolean use_shader = false);
        // Sets a Vector2 in the shader
        void SetVector2f (const GLchar* name, GLfloat x, GLfloat y, GLboolean use_shader = false);
        void SetVector2f (const GLchar* name, const glm::vec2& value, GLboolean use_shader = false);
        // Sets a Vector3 in the shader
        void SetVector3f (const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLboolean use_shader = false);
        void SetVector3f (const GLchar* name, const glm::vec3& value, GLboolean use_shader = false);
        // Sets a Vector4 in the shader
        void SetVector4f (const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean use_shader = false);
        void SetVector4f (const GLchar* name, const glm::vec4& value, GLboolean use_shader = false);
        // Sets a matrix in the shader
        void SetMatrix4 (const GLchar* name, const glm::mat4& matrix, GLboolean use_shader = false);
    private:
        // ID of the shader
        GLuint ID;
        // checks for compilation errors
        void CheckCompileErrors(GLuint object, std::string type); 
    };
}