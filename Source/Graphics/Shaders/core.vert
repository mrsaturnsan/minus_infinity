#version 330 core

layout (location = 0) in vec3 pos;
layout (location = 1) in mat4 model;
layout (location = 5) in vec3 normal;
layout (location = 6) in vec2 texcoord;

uniform mat4 proj_view;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoord;

void main()
{
    // set the texture coordinates
    TexCoord = texcoord;
    // calculate fragment position
    FragPos = vec3(model * vec4(pos, 1.0));
    // calculate the lighting normal
    Normal = mat3(transpose(inverse(model))) * normal;
    // set position
    gl_Position = proj_view * model * vec4(pos, 1.0);
}