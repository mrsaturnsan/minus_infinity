#version 330 core

out vec4 FragColor;
in vec2 TexCoords;

uniform sampler2D screenTexture;
uniform bool magnify = true;
uniform float deltaTime;
uniform float totalTime;

const float window_factor = 0;

float distance_from_edge(vec2 vec)
{
    // top right
    if (vec.x >= 0 && vec.y >= 0)
    {
        return ((window_factor - vec.x) * (window_factor - vec.x)) + ((window_factor - vec.y) * (window_factor - vec.y));
    }
    else if (vec.x < 0 && vec.y >= 0) // top left
    {
        return ((-window_factor - vec.x) * (-window_factor - vec.x)) + ((window_factor - vec.y) * (window_factor - vec.y));
    }
    else if (vec.x < 0 && vec.y < 0) // bottom left
    {
        return ((-window_factor - vec.x) * (-window_factor - vec.x)) + ((-window_factor - vec.y) * (-window_factor - vec.y));
    }
    else // bottom right
    {
        return ((window_factor - vec.x) * (window_factor - vec.x)) + ((-window_factor - vec.y) * (-window_factor - vec.y));
    }
}

void main()
{
    vec2 tcadj = TexCoords - vec2(0.5f, 0.5f);
    float dist = distance_from_edge(tcadj);

    if (magnify)
    {
        if (dist < 0.24f)
        {
            // area inside the lens
            vec2 texcoord = TexCoords;
            texcoord.y += cos(texcoord.x * 15 * 3 * 3.14159 + deltaTime) / 300;
            FragColor = texture(screenTexture, texcoord * 0.5f);
        }
        else
        {      
            FragColor = texture(screenTexture, TexCoords) - vec4(0.7f, 0.7f, 0.7f, 0);  
        } 
    }
    else
    {
        FragColor = texture(screenTexture, TexCoords);
    }
} 