#version 330 core

out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;  
in vec2 TexCoord;
  
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 objectColor;

uniform sampler2D my_texture;

void main()
{
    // ambient
    float ambientStrength = 0.3;
    vec3 ambient = ambientStrength * lightColor;
  	
    // diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    
    // calculate the resultant color
    vec3 result = (ambient + diffuse);

    // write the color in
    FragColor = texture(my_texture, TexCoord) * vec4(result, 1.0);
    //FragColor = texture(my_texture, TexCoord);
}