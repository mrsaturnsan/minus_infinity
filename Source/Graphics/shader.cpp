/******************************************************************************/
/*
* @file   shader.cpp
* @author Aditya Harsh
* @brief  Resposible for managing shaders.
*/
/******************************************************************************/

#include "shader.h"

namespace mifty
{
    /**
     * @brief Compiles a shader
     * 
     * @param vertexSource 
     * @param fragmentSource 
     * @param geometrySource 
     */
    void Shader::Compile(const GLchar* vertexSource, const GLchar* fragmentSource, const GLchar* geometrySource)
    {
        GLuint sVertex = 0, sFragment = 0, gShader = 0;

        // Vertex Shader
        sVertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(sVertex, 1, &vertexSource, NULL);
        glCompileShader(sVertex);
        CheckCompileErrors(sVertex, "VERTEX");

        // Fragment Shader
        sFragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(sFragment, 1, &fragmentSource, NULL);
        glCompileShader(sFragment);
        CheckCompileErrors(sFragment, "FRAGMENT");

        // If geometry shader source code is given, also compile geometry shader
        if (geometrySource != nullptr)
        {
            gShader = glCreateShader(GL_GEOMETRY_SHADER);
            glShaderSource(gShader, 1, &geometrySource, NULL);
            glCompileShader(gShader);
            CheckCompileErrors(gShader, "GEOMETRY");
        }

        // Shader Program
        this->ID = glCreateProgram();
        glAttachShader(this->ID, sVertex);
        glAttachShader(this->ID, sFragment);

        if (geometrySource != nullptr)
            glAttachShader(this->ID, gShader);

        glLinkProgram(this->ID);
        CheckCompileErrors(this->ID, "PROGRAM");

        // Delete the shaders as they're linked into our program now and no longer necessery
        glDeleteShader(sVertex);
        glDeleteShader(sFragment);

        if (geometrySource != nullptr)
            glDeleteShader(gShader);
    }

    /**
     * @brief Activates a shader
     * 
     */
    void Shader::Use()
    {
        glUseProgram(this->ID);
    }

    /**
     * @brief Sets a uniform float value
     * 
     * @param name 
     * @param value 
     * @param useShader 
     */
    void Shader::SetFloat(const GLchar* name, GLfloat value, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniform1f(glGetUniformLocation(this->ID, name), value);
    }

    /**
     * @brief Sets a uniform int value
     * 
     * @param name 
     * @param value 
     * @param useShader 
     */
    void Shader::SetInteger(const GLchar* name, GLint value, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniform1i(glGetUniformLocation(this->ID, name), value);
    }

    /**
     * @brief Sets a uniform Vector2 value
     * 
     * @param name 
     * @param x 
     * @param y 
     * @param useShader 
     */
    void Shader::SetVector2f(const GLchar* name, GLfloat x, GLfloat y, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniform2f(glGetUniformLocation(this->ID, name), x, y);
    }

    /**
     * @brief Sets a uniform Vector2 value
     * 
     * @param name 
     * @param value 
     * @param useShader 
     */
    void Shader::SetVector2f(const GLchar* name, const glm::vec2 &value, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniform2f(glGetUniformLocation(this->ID, name), value.x, value.y);
    }

    /**
     * @brief Sets a uniform Vector3 value
     * 
     * @param name 
     * @param x 
     * @param y 
     * @param z 
     * @param useShader 
     */
    void Shader::SetVector3f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniform3f(glGetUniformLocation(this->ID, name), x, y, z);
    }

    /**
     * @brief Sets a uniform Vector3 value
     * 
     * @param name 
     * @param value 
     * @param useShader 
     */
    void Shader::SetVector3f(const GLchar* name, const glm::vec3 &value, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniform3f(glGetUniformLocation(this->ID, name), value.x, value.y, value.z);
    }

    /**
     * @brief Sets a uniform Vector4 value
     * 
     * @param name 
     * @param x 
     * @param y 
     * @param z 
     * @param w 
     * @param useShader 
     */
    void Shader::SetVector4f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniform4f(glGetUniformLocation(this->ID, name), x, y, z, w);
    }

    /**
     * @brief Sets a uniform Vector4 value
     * 
     * @param name 
     * @param value 
     * @param useShader 
     */
    void Shader::SetVector4f(const GLchar* name, const glm::vec4 &value, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniform4f(glGetUniformLocation(this->ID, name), value.x, value.y, value.z, value.w);
    }

    /**
     * @brief Sets a uniform Matrix4 value
     * 
     * @param name 
     * @param matrix 
     * @param useShader 
     */
    void Shader::SetMatrix4(const GLchar* name, const glm::mat4 &matrix, GLboolean useShader)
    {
        if (useShader)
            this->Use();
        glUniformMatrix4fv(glGetUniformLocation(this->ID, name), 1, GL_FALSE, glm::value_ptr(matrix));
    }

    /**
     * @brief Checks for any compilation errors
     * 
     * @param object 
     * @param type 
     */
    void Shader::CheckCompileErrors(GLuint object, std::string type)
    {
        GLint success;
        GLchar infoLog[1024];
        if (type != "PROGRAM")
        {
            glGetShaderiv(object, GL_COMPILE_STATUS, &success);
            if (!success)
            {
                glGetShaderInfoLog(object, 1024, NULL, infoLog);
                std::cout << "| ERROR::SHADER: Compile-time error: Type: " << type << "\n"
                    << infoLog << "\n -- --------------------------------------------------- -- "
                    << std::endl;
            }
        }
        else
        {
            glGetProgramiv(object, GL_LINK_STATUS, &success);
            if (!success)
            {
                glGetProgramInfoLog(object, 1024, NULL, infoLog);
                std::cout << "| ERROR::Shader: Link-time error: Type: " << type << "\n"
                    << infoLog << "\n -- --------------------------------------------------- -- "
                    << std::endl;
            }
        }
    }
}