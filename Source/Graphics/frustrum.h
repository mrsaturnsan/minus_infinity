/******************************************************************************/
/*
* @file   frustrum.h
* @author Aditya Harsh
* @brief  Responsible for view-frustrum culling
*/
/******************************************************************************/

#pragma once

#include "meshmanager.h"

namespace mifty
{
    class Frustrum
    {
    public:
        static bool CheckFrustrum(MeshID ID, const glm::mat4& object, const glm::mat4& camera);
    private:
        // static class
        Frustrum() = delete;
        static bool CheckCube(const glm::mat4& object, const glm::mat4& camera) noexcept;
    };
}