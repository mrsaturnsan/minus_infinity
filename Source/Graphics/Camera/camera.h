/******************************************************************************/
/*
* @file   camera.h
* @author Aditya Harsh
* @brief  Static camera class
*/
/******************************************************************************/

#pragma once

#include "component.h"

namespace mifty
{
    namespace
    {
        // Default camera values
        const float YAW        = -60.0f;
        const float PITCH      =  0.0f;
        const float SENSITIVTY =  0.3f;
        const float ZOOM       =  45.0f;
    }

    // forward declare
    class Actor;
    class Graphics;

    class Camera : public Component
    {
        // allow graphics to call UpdateProjMat
        friend Graphics;
    public:
        // TODO: Write getters/setters for these guys
        glm::vec3 pos_;
        glm::vec3 front_;
        glm::vec3 up_;
        glm::vec3 right_;
        glm::vec3 world_up_;

        Camera(Actor* parent, float ratio, const glm::vec3& position = glm::vec3(0.0f, 0.0f, 0.0f), const glm::vec3& up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH, float min = 0.1f, float max = 1000.0f);
        // destructor
        virtual ~Camera();
        // gets the view matrix
        glm::mat4 GetViewMatrix() noexcept;
        // gets the projection matrix
        const glm::mat4& GetProjMat() const noexcept;
        // adds position to the camera
        void AddPosition(const glm::vec3& pos) noexcept;
        // sets the camer aposition
        void SetPosition(const glm::vec3& pos) noexcept;
        // gets the position of the camera
        const glm::vec3& GetPosition() noexcept;
        // gets the zoom
        float GetZoom() noexcept;
        // pure virtual
        virtual void Update() override;
        // Handles camera rotations
        void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true);
        // pure virtual
        virtual std::unique_ptr<Component> Clone() override;
        // returns the active camera within the scene (singleton)
        static Camera* GetActiveCamera();
        static float GetMin() noexcept;
        static float GetMax() noexcept;
    private:
        // recalculates vectors
        void UpdateCameraVectors();
        // recalculates projection matrix
        void UpdateProjecMat(float ratio);

        // Eular Angles
        float yaw_;
        float pitch_;

        // Camera options
        float mouse_sensitivity_;
        float zoom_;

        // projection matrix
        float min_;
        float max_;
        glm::mat4 proj_;
    };
}