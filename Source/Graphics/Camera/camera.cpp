/******************************************************************************/
/*
* @file   camera.cpp
* @author Aditya Harsh
* @brief  Static camera class
*/
/******************************************************************************/

#include "camera.h"
#include "utility.h"
#include "actor.h"
#include "graphics.h"

namespace mifty
{
    namespace
    {
        // the current active camera
        Camera* active_camera = nullptr;
    }

    /**
     * @brief Camera constructor
     * 
     */
    Camera::Camera(Actor* parent, float ratio, const glm::vec3& position, const glm::vec3& up, float yaw, float pitch, float min, float max) : Component(parent), front_(glm::vec3(0.0f, 0.0f, -1.0f)), mouse_sensitivity_(SENSITIVTY), zoom_(ZOOM)
    {
        // destroy the active camera if it exists
        if (active_camera)
        {
            active_camera->GetParent()->SetDestroy(true);
            std::cout << "Existing camera is set to be destroyed. Was this intentional?\n";
        }

        // replace singleton
        active_camera = this;

        // set values
        pos_ = position;
        world_up_ = up;
        yaw_ = yaw;
        pitch_ = pitch;
        min_ = min;
        max_ = max;

        // update vectors
        UpdateCameraVectors();
        // update matrix
        UpdateProjecMat(ratio);
    }

    /**
     * @brief Destructor for camera
     * 
     */
    Camera::~Camera()
    {
        // there should only be one camera...
        active_camera = nullptr;
    }

    /**
     * @brief Gets the view matrix
     * 
     * @return glm::mat4 
     */
    glm::mat4 Camera::GetViewMatrix() noexcept
    {
        return glm::lookAt(pos_, pos_ + front_, up_);
    }

    /**
     * @brief Gets the projection matrix
     * 
     * @return const glm::mat4& Camera::GetProjMat const 
     */
    const glm::mat4& Camera::GetProjMat() const noexcept
    {
        return proj_;
    }

    /**
     * @brief Adds position to the camera
     * 
     * @param pos 
     */
    void Camera::AddPosition(const glm::vec3& pos) noexcept
    {
        pos_ += pos;
    }

    /**
     * @brief Directly sets the position of the camera
     * 
     * @param glm::vec3 
     */
    void Camera::SetPosition(const glm::vec3& pos) noexcept
    {
        pos_ = pos;
    }

    /**
     * @brief Gets the current position of the camera
     * 
     * @return const glm::vec3& Camera::GetPosition 
     */
    const glm::vec3& Camera::GetPosition() noexcept
    {
        return pos_;
    }

    /**
     * @brief Gets the zoom
     * 
     * @return float Camera::GetZoom 
     */
    float Camera::GetZoom() noexcept
    {
        return zoom_;
    }

    /**
     * @brief Handles camera movement
     * 
     */
    void Camera::Update()
    {
        
    }

    /**
     * @brief Handles camera rotation
     * 
     * @param xoffset 
     * @param yoffset 
     * @param constrainPitch 
     */
    void Camera::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch)
    {
        xoffset *= mouse_sensitivity_;
        yoffset *= mouse_sensitivity_;

        yaw_   += xoffset;
        pitch_ += yoffset;

        // Make sure that when pitch is out of bounds, screen doesn't get flipped
        if (constrainPitch)
        {
            if (pitch_ > 89.0f)
                pitch_ = 89.0f;
            if (pitch_ < -89.0f)
                pitch_ = -89.0f;
        }

        // Update Front, Right and Up Vectors using the updated Eular angles
        UpdateCameraVectors();
    }

    /**
     * @brief Clones the camera component
     * 
     * @return std::unique_ptr<Component> 
     */
    std::unique_ptr<Component> Camera::Clone()
    {
        return std::make_unique<Camera>(parent_, Graphics::GetWindowRatio(), pos_, up_, yaw_, pitch_);
    }

    /**
     * @brief Returns the singleton camera
     * 
     * @return Camera* 
     */
    Camera* Camera::GetActiveCamera()
    {
        return active_camera;
    }

    /**
     * @brief Recalculates camera vectors
     * 
     */
    void Camera::UpdateCameraVectors()
    {
        // calculate new front vector
        glm::vec3 front;
        front.x = cos(glm::radians(yaw_)) * cos(glm::radians(pitch_));
        front.y = sin(glm::radians(pitch_));
        front.z = sin(glm::radians(yaw_)) * cos(glm::radians(pitch_));

        // recalculate vectors
        front_ = glm::normalize(front);
        right_ = glm::normalize(glm::cross(front_, world_up_));
        up_ = glm::normalize(glm::cross(right_, front_));
    }

    /**
     * @brief Updates the projection matrix
     * 
     */
    void Camera::UpdateProjecMat(float ratio)
    {
        proj_ = glm::perspective(glm::radians(zoom_), ratio, min_, max_);
    }

    /**
     * @brief Gets the minimum range of the camera
     * 
     * @return float Camera::GetMin const 
     */
    float Camera::GetMin() noexcept
    {
        return active_camera->min_;
    }

    /**
     * @brief Gets the maximum of a camera
     * 
     * @return float Camera::GetMax const 
     */
    float Camera::GetMax() noexcept
    {
        return active_camera->max_;
    }
}