/******************************************************************************/
/*
* @file   frustrum.cpp
* @author Aditya Harsh
* @brief  Responsible for view-frustrum culling
*/
/******************************************************************************/

#include "frustrum.h"
#include "input.h"
#include "graphics.h"
#include "camera.h"

namespace mifty
{
    /**
     * @brief Call to check whether or not an object is inside the view frustrum
     * 
     * @param ID 
     * @param object 
     * @param camera 
     * @return true 
     * @return false 
     */
    bool Frustrum::CheckFrustrum(MeshID ID, const glm::mat4& object, const glm::mat4& camera)
    {
        switch (ID)
        {
            case MeshID::CUBE:
                return CheckCube(object, camera);
            case MeshID::PLANE:
                // TODO: Add plane checking
                break;
        }
        
        throw std::runtime_error("Something bad happened!");
    }
    
    /**
     * @brief Check if a cube is within the view frustrum
     * 
     * @param object 
     * @param camera 
     * @return true 
     * @return false 
     */
    bool Frustrum::CheckCube(const glm::mat4& object, const glm::mat4& camera) noexcept
    {
        // create the matrix
        const glm::mat4 o2c = camera * object;

        // create 8 points, then transform
        std::array<glm::vec4, 8> points
        {
            glm::vec4(o2c * glm::vec4(-1.0f, -1.0f , 1.0f, 1.0f)),
            glm::vec4(o2c * glm::vec4(1.0f, -1.0f , 1.0f, 1.0f)),
            glm::vec4(o2c * glm::vec4(1.0f, 1.0f , 1.0f, 1.0f)),
            glm::vec4(o2c * glm::vec4(-1.0f, 1.0f , 1.0f, 1.0f)),
            glm::vec4(o2c * glm::vec4(-1.0f, -1.0f , -1.0f, 1.0f)),
            glm::vec4(o2c * glm::vec4(1.0f, -1.0f , -1.0f, 1.0f)),
            glm::vec4(o2c * glm::vec4(1.0f, 1.0f , -1.0f, 1.0f)),
            glm::vec4(o2c * glm::vec4(-1.0f, 1.0f , -1.0f, 1.0f))
        };

        // loop through each point
        for (unsigned i = 0; i < 8; ++i)
            // check if the point is visible (can't be culled if even a single point is visible)
            if (abs(points[i].x) < points[i].w && abs(points[i].y) < points[i].w &&  points[i].z > 0 && points[i].z < points[i].w)
                 return true;   
                 
        // outside of the view frustrum
        return false;
    }
}