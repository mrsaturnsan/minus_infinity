/******************************************************************************/
/*
* @file   graphics.cpp
* @author Aditya Harsh
* @brief  Contains core graphics.
*/
/******************************************************************************/

#include "graphics.h"
#include "utility.h"
#include "materialmanager.h"
#include "camera.h"
#include "input.h"
#include "optionparse.h"
#include "postprocess.h"

namespace mifty
{
    namespace
    {
        // pointer to the current window
        GLFWwindow* window;
        // the size of the window in pixels
        glm::i32vec2 window_size(900, 600);
    }

    /**
     * @brief Initializes the core graphics engine
     * 
     */
    void Graphics::InitGraphics()
    {
        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    #ifdef _DEBUG
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_FALSE);
    #endif

        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

        // Create the window
        window = glfwCreateWindow(window_size.x, window_size.y, "Game Engine", nullptr, nullptr);
        
        if (!window)
        {
            glfwTerminate();
            throw std::runtime_error("Failed to create a window");
        }

        // Create the viewport
        glViewport(0, 0, window_size.x, window_size.y);

        // Set the context
        glfwMakeContextCurrent(window);

        // Set input callbacks
        glfwSetKeyCallback(window, Input::InputCallback);
        glfwSetMouseButtonCallback(window, Input::MouseButtonCallBack);
        glfwSetCursorPosCallback(window, Input::MouseCallBack);
        glfwSetScrollCallback(window, Input::ScrollCallback);

        // callback for window size changes
        glfwSetFramebufferSizeCallback(window, FrameBufferCallBack);

        // Get the size of the screen
        glfwGetFramebufferSize(window, &window_size.x, &window_size.y);

        // tell GLFW to capture the mouse
        if (OptionParse::LockMouse())
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        // Enable additional OpenGL features
        glewExperimental = GL_TRUE;

        glewInit();
        glGetError(); // Call it once to catch glewInit() bug

    #ifdef _DEBUG
        // Enable GL error callback
        GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
        if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
        {
            glEnable(GL_DEBUG_OUTPUT);
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
            glDebugMessageCallback(GLDebugOutput, nullptr);
            glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
        }
    #endif

        // configure settings
        Config();
    }

    /**
     * @brief Renders graphics to the screen
     * 
     */
    void Graphics::Render()
    {
        // clear the buffers
        Clear(1.0f, 1.0f, 0.94f, 1.0f);

        // render objects here
        MaterialManager::DrawMaterials();

        // actually draw
        PostProcess::Draw();
        
        // swap the frame buffer
        SwapFrameBuffers();
    }

    /**
     * @brief Initial OpenGL settings
     * 
     */
    void Graphics::Config()
    {
        // OpenGL configurations 
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        //glEnable(GL_FRAMEBUFFER_SRGB);
    }

    /**
     * @brief Swaps frame buffers
     * 
     */
    void Graphics::SwapFrameBuffers()
    {
        // Swap the render buffer
        glfwSwapBuffers(window);
    }

    /**
     * @brief Clears the background color
     * 
     * @param r Red
     * @param g Green
     * @param b Blue
     * @param a Alpha
     */
    void Graphics::Clear(GLfloat r, GLfloat g, GLfloat b, GLfloat a)
    {
        // Add background color
        glClearColor(r, g, b, a);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    /**
     * @brief Polls OpenGL for events
     * 
     */
    void Graphics::PollEvents()
    {
        glfwPollEvents();
    }

    /**
     * @brief Whether or not the exit button was clicked
     * 
     * @return true 
     * @return false 
     */
    bool Graphics::ShouldClose()
    {
        return glfwWindowShouldClose(window) || Input::InputCurrent(GLFW_KEY_ESCAPE);
    }

    /**
     * @brief Shuts down the graphics
     * 
     */
    void Graphics::ShutDown()
    {
        // Shut down OpenGl
        glfwTerminate();
    }

    /**
     * @brief Called whenever something happens to the frame buffer
     * 
     * @param window Pointer to the window
     * @param width New width
     * @param height New height
     */
    void Graphics::FrameBufferCallBack(GLFWwindow* window, int width, int height)
    {
        // discard window
        UNUSED(window);

        // update the window size
        window_size.x = width;
        window_size.y = height;

        // Change viewport size
        glViewport(0, 0, width, height);

        // Update the active camera ratio if it exists
        Camera* cam = Camera::GetActiveCamera();
        if (cam)
            cam->UpdateProjecMat(GetWindowRatio());

        PostProcess::Resize(glm::i32vec2(width, height));
    }

    /**
     * @brief Function that's called whenever OpenGL encounters an error
     * 
     * @param source Source of the error
     * @param type Type of the error
     * @param id  ID
     * @param severity How severe the error is
     * @param length The length of the error
     * @param message Message passed in
     * @param userParam Any parameters
     */
    void APIENTRY Graphics::GLDebugOutput(GLenum source,
        GLenum type,
        GLuint id,
        GLenum severity,
        GLsizei length,
        const GLchar *message,
        const void *userParam)
    {
        UNUSED(length);
        UNUSED(userParam);

        // ignore non-significant error/warning codes
        if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

        std::cout << "---------------" << '\n';
        std::cout << "Debug message (" << id << "): " << message << '\n';

        switch (source)
        {
        case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
        case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
        case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
        } std::cout << '\n';

        switch (type)
        {
        case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
        case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
        case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
        case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
        case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
        case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
        case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
        } std::cout << '\n';

        switch (severity)
        {
        case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
        case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
        case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
        } std::cout << '\n';
        std::cout << '\n';
    }

    /**
     * @brief Gets the ration
     * 
     * @return float Graphics::GetWindowRatio
     */
    float Graphics::GetWindowRatio()
    {
        if (window_size.y == 0)
            throw std::runtime_error("Attempting to divide by zero");
        return static_cast<float>(window_size.x) / static_cast<float>(window_size.y);
    }

    /**
     * @brief Returns the size of the window
     * 
     * @return const glm::i32vec2& Graphics::GetWindowSize 
     */
    const glm::i32vec2& Graphics::GetWindowSize() noexcept
    {
        return window_size;
    }
}