/******************************************************************************/
/*
* @file   graphics.h
* @author Aditya Harsh
* @brief  Contains core graphics.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    /**
     * @brief Contains core graphics logic/interface
     * 
     */
    class Graphics
    {
    public:
        // initialize the graphics engine
        static void InitGraphics();
        // renders graphics
        static void Render();  
        // polls events
        static void PollEvents();
        // whether or not the window should close
        static bool ShouldClose();
        // shuts down graphics
        static void ShutDown();
        // gets the ratio of the window
        static float GetWindowRatio();
        // gets the window size
        static const glm::i32vec2& GetWindowSize() noexcept;
    private:
        Graphics() = delete;
        // OpenGL Config
        static void Config();
        // swap frame buffers on an object
        static void SwapFrameBuffers();
        // clears the background color
        static void Clear(GLfloat r, GLfloat g, GLfloat b, GLfloat a);
        // called whenever some change occurs to the window
        static void FrameBufferCallBack(GLFWwindow* window, int width, int height);
        // called whenever OpenGL encounters an error
        static void APIENTRY GLDebugOutput(GLenum source,
        GLenum type,
        GLuint id,
        GLenum severity,
        GLsizei length,
        const GLchar *message,
        const void *userParam);
    };
}