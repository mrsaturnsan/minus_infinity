/******************************************************************************/
/*
* @file   light.h
* @author Aditya Harsh
* @brief  Handles light objects in the scene.
*/
/******************************************************************************/

#pragma once

#include "component.h"

namespace mifty
{
    // forward declare
    class Actor;

    /**
     * @brief The light class
     * 
     */
    class Light : public Component
    {
    public:
        Light(Actor* actor, const glm::vec3& pos, const glm::vec3& color);
        virtual ~Light();
        virtual void Update() override;
        virtual std::unique_ptr<Component> Clone() override;
        void AddPosition(const glm::vec3& pos) noexcept;
        void SetPosition(const glm::vec3& pos) noexcept;
        const glm::vec3& GetPosition() const noexcept;
        void SetColor(const glm::vec3& col) noexcept;
        const glm::vec3& GetColor() const noexcept;
        static Light* GetActiveLight() noexcept;
    private:
        glm::vec3 pos_;
        glm::vec3 color_;
    };
}