/******************************************************************************/
/*
* @file   mat_vertex_color.h
* @author Aditya Harsh
* @brief  Simple vertex color material
*/
/******************************************************************************/

#pragma once

#include "material.h"
#include "meshmanager.h"
#include "texturemanager.h"

namespace mifty
{
    /**
     * @brief Inherits from the base material class
     * 
     */
    class MatVertexColor : public Material
    {
    public:
        // default constructor
        MatVertexColor();
        
        // destructor
        virtual ~MatVertexColor() override;
        // overrides the Draw function in the base class
        virtual void Draw() override;
        // update function
        virtual void Update(Render& rdr) override;
        // gets the shader
        virtual Shader& GetShader() noexcept override;
    private:
        // the map of matrices to be drawn
        std::map<MeshID, std::map<TextureType, std::vector<glm::mat4>>> matrices_;
        // the VBO for the instance
        GLuint IVBO_;
    };
}