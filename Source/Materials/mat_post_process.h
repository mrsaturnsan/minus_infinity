/******************************************************************************/
/*
* @file   mat_post_process.h
* @author Aditya Harsh
* @brief  Post-processor material
*/
/******************************************************************************/

#pragma once

#include "material.h"
#include "meshmanager.h"

namespace mifty
{
    /**
     * @brief Inherits from the base material class
     * 
     */
    class MatPostProcess: public Material
    {
    public:
        // constructor
        MatPostProcess();
        // destructor
        virtual ~MatPostProcess() override;
        // overrides the Draw function in the base class
        virtual void Draw() override;
        // update function
        virtual void Update(Render& rdr) override;
        // gets the shader
        virtual Shader& GetShader() noexcept override;
    };
}