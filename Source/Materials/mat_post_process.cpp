/******************************************************************************/
/*
* @file   mat_post_process.cpp
* @author Aditya Harsh
* @brief  Post-processor material
*/
/******************************************************************************/

#include "mat_post_process.h"
#include "utility.h"

namespace mifty
{
    /**
     * @brief Constructor
     * 
     */
    MatPostProcess::MatPostProcess() : Material("Graphics/Shaders/postprocess.vert", "Graphics/Shaders/postprocess.frag")
    {

    }

    /**
     * @brief Destructor
     * 
     */
    MatPostProcess::~MatPostProcess()
    {
        
    }

    /**
     * @brief Draw function
     * 
     */
    void MatPostProcess::Draw()
    {
        shader_.Use();
    }

    /**
     * @brief Update function
     * 
     * @param rdr 
     */
    void MatPostProcess::Update(Render& rdr)
    {
        UNUSED(rdr);
    }

    /**
     * @brief Gets the shader off of a material
     * 
     * @return Shader& 
     */
     Shader& MatPostProcess::GetShader() noexcept
    {
        return shader_;
    }
}