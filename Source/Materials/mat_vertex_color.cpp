/******************************************************************************/
/*
* @file   mat_vertex_color.cpp
* @author Aditya Harsh
* @brief  Simple vertex color material
*/
/******************************************************************************/

#include "mat_vertex_color.h"
#include "camera.h"
#include "shader.h"
#include "render.h"
#include "actor.h"
#include "transform.h"
#include "light.h"
#include "gametime.h"
#include "frustrum.h"
#include "texturemanager.h"
#include "jobmanager.h"

namespace mifty
{
    /**
     * @brief Default constructor
     * 
     */
    MatVertexColor::MatVertexColor() : Material("Graphics/Shaders/core.vert", "Graphics/Shaders/core.frag")
    {
        // generate the instance vbo
        glGenBuffers(1, &IVBO_);
    }

    /**
     * @brief Virtual destructor
     * 
     */
    MatVertexColor::~MatVertexColor()
    {
        // delete the instance buffer
        glDeleteBuffers(1, &IVBO_);
    }

    /**
     * @brief The draw function uploads a batch of data to the GPU
     * 
     */
    void MatVertexColor::Draw()
    {
        // break out if there's nothing to be drawn
        if (matrices_.empty()) return;

        // active the shader for the material
        shader_.Use();

        // calculate the camera to screen matrix
        glm::mat4 cam_scr = Camera::GetActiveCamera()->GetProjMat() * Camera::GetActiveCamera()->GetViewMatrix();
        
        // set uniforms within the shader
        shader_.SetMatrix4("proj_view", cam_scr);

        shader_.SetVector3f("lightPos", Light::GetActiveLight()->GetPosition());
        shader_.SetVector3f("lightColor", Light::GetActiveLight()->GetColor());
        
        //shader_.SetVector3f("objectColor", glm::vec3(0.69f, 1.0f, 0.6f));
        shader_.SetVector3f("objectColor", glm::vec3(1.0f, 0.0f, 0.0f));


        // loop through each element in the map
        for (auto& i : matrices_)
        {
            // remove elements that are outside of the view frustrum
            // i.second.erase(std::remove_if(std::begin(i.second), std::end(i.second), [&i, &cam_scr] (const auto& trs)
            // {
            //     return !Frustrum::CheckFrustrum(i.first, trs, cam_scr);
            // }), std::end(i.second));

            // draw by textures
            for (auto& j : i.second)
            {
                if (j.second.empty())
                    continue;

                // get the data
                const auto& data = MeshManager::GetMesh(i.first);

                // bind the vertex array
                glBindVertexArray(std::get<0>(data));

                // set up attribute pointers
                glEnableVertexAttribArray(1);
                glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
                glEnableVertexAttribArray(2);
                glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
                glEnableVertexAttribArray(3);
                glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
                glEnableVertexAttribArray(4);
                glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));
            
                // set the divisors
                glVertexAttribDivisor(1, 1);
                glVertexAttribDivisor(2, 1);
                glVertexAttribDivisor(3, 1);
                glVertexAttribDivisor(4, 1);

                // figure out how many indices are being used
                int size;
                glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
                size /= sizeof(GLushort);

                // bind the instance VBO
                glBindBuffer(GL_ARRAY_BUFFER, IVBO_);

                // buffer data into it
                glBufferData(GL_ARRAY_BUFFER, (sizeof(glm::mat4) * j.second.size()), j.second.data(), GL_STATIC_DRAW);

                // bind texture
                if (j.first != TextureType::NONE)
                    TextureManager::GetTexture(j.first).Bind();

                // upload data to the GPU
                glDrawElementsInstanced(GL_TRIANGLES, size, GL_UNSIGNED_SHORT, 0, GLsizei(j.second.size()));

                // disable the attribute arrays
                glDisableVertexAttribArray(1);
                glDisableVertexAttribArray(2);
                glDisableVertexAttribArray(3);
                glDisableVertexAttribArray(4);
                
                // unbind the vertex array
                glBindVertexArray(0);
            }
        }

        // clear the map
        matrices_.clear();
    }

    /**
     * @brief Update function (virtual)
     * 
     * @param rdr 
     */
    void MatVertexColor::Update(Render& rdr)
    {
        // get the transform component
        Transform* trs = rdr.GetParent()->GetComponent<Transform>();

        if (!trs)
            throw std::runtime_error("Transform component missing!");
            
        // push data
        matrices_[rdr.GetMesh()][rdr.GetTexture()].emplace_back(trs->GetMatrix());
    }

    /**
     * @brief Gets the shader on a material.
     * 
     * @return Shader& MatVertexColor::GetShader noexcept 
     */
    Shader& MatVertexColor::GetShader() noexcept
    {
        return shader_;
    }
}