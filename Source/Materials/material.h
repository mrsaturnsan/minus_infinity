/******************************************************************************/
/*
* @file   material.h
* @author Aditya Harsh
* @brief  Abstract material class which allows different materials to share
          a common interface
*/
/******************************************************************************/

#pragma once

#include "shader.h"

namespace mifty
{
    // forward declare
    class Render;

    /**
     * @brief Material class defining the structure of other materials
     * 
     */
    class Material
    {
    public:
        // constructor
        Material(const GLchar* vshaderfile, const GLchar* fshaderfile, const GLchar* gshaderfile = nullptr);
        // allows the destructor within each material object to be called
        virtual ~Material() = 0;
        // the draw function uploads material data to the GPU
        virtual void Draw() = 0;
        // updates data within the material
        virtual void Update(Render& rnd) = 0;
        // returns the shader
        virtual Shader& GetShader() noexcept = 0;
    protected:
        Shader shader_;
    };
}