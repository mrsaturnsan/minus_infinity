/******************************************************************************/
/*
* @file   material.cpp
* @author Aditya Harsh
* @brief  Abstract material class which allows different materials to share
          a common interface
*/
/******************************************************************************/

#include "material.h"

namespace mifty
{

    /**
     * @brief Constructor for materials
     * 
     * @param shader 
     */
    Material::Material(const GLchar* vshaderfile, const GLchar* fshaderfile, const GLchar* gshaderfile)
    {
        // strings with data for each source file
        std::string vertexcode;
        std::string fragmentcode;
        std::string geometrycode;
        
        try
        {
            // Open files
            std::ifstream vertexshaderfile(vshaderfile);
            std::ifstream fragmentshaderfile(fshaderfile);
            std::stringstream vshaderstream, fshaderstream;

            // Read file's buffer contents into streams
            vshaderstream << vertexshaderfile.rdbuf();
            fshaderstream << fragmentshaderfile.rdbuf();

            // close file handlers
            vertexshaderfile.close();
            fragmentshaderfile.close();

            // Convert stream into string
            vertexcode = vshaderstream.str();
            fragmentcode = fshaderstream.str();

            // If geometry shader path is present, also load a geometry shader
            if (gshaderfile != nullptr)
            {
                std::ifstream geometryshaderfile(gshaderfile);
                std::stringstream gshaderstream;
                gshaderstream << geometryshaderfile.rdbuf();
                geometryshaderfile.close();
                geometrycode = gshaderstream.str();
            }
        }
        catch (const std::exception& e)
        {
            std::cout << "ERROR::SHADER: Failed to read shader files\n";
        }

        // compile the shader
        shader_.Compile(vertexcode.c_str(), fragmentcode.c_str(), gshaderfile != nullptr ? geometrycode.c_str() : nullptr);
    }

    /**
     * @brief Virtual destructor
     * 
     */
    Material::~Material()
    {
        
    }
}