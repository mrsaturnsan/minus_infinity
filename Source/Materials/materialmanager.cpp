/******************************************************************************/
/*
* @file   materialmanager.cpp
* @author Aditya Harsh
* @brief  Manages the materials within the scene
*/
/******************************************************************************/

#include "materialmanager.h"

// Add new materials here
#include "mat_vertex_color.h"

namespace mifty
{
    namespace
    {
        // static map of all the materials that exist within the scene
        std::map<MaterialType, std::unique_ptr<Material>> materials;
    }

    /**
     * @brief Loads a material into memory
     * 
     * @param type 
     */
    void MaterialManager::LoadMaterial(MaterialType type)
    {
        if (materials.find(type) == materials.end())
        {
            switch (type)
            {
            case MaterialType::MAT_VERT_COL:
                materials[type] = std::move(std::make_unique<MatVertexColor>());
                break;
            default:
                std::cout << "Invalid material type\n";
            }
        }
    }

    /**
     * @brief Removes a material from memory
     * 
     * @param type 
     */
    void MaterialManager::RemoveMaterial(MaterialType type)
    {
        if (materials.find(type) == materials.end())
            materials.erase(type);
    }

    /**
     * @brief Draws all materials
     * 
     */
    void MaterialManager::DrawMaterials()
    {
        for (auto& it : materials)
            it.second.get()->Draw();
    }

    /**
     * @brief Gets a pointer to a material
     * 
     * @param type 
     * @return Material&
     */
    Material& MaterialManager::GetMaterial(MaterialType type)
    {
        if (materials.find(type) != materials.end())
            return *materials[type].get();
        throw std::runtime_error("Material not created");
    }

    /**
     * @brief Removes all of the materials from memory
     * 
     */
    void MaterialManager::RemoveAllMaterials()
    {
        materials.clear();
    }
}