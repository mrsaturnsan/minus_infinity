/******************************************************************************/
/*
* @file   worldgen.h
* @author Aditya Harsh
* @brief  Generates the world.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    // forward declare
    class Actor;

    /**
     * @brief In charge of keep track of the world and generating it.
     * 
     */
    class WorldGenerator
    {
    public:
        static void CreateWorld();
    private:
        // static class
        WorldGenerator() = delete;
    };
}