/******************************************************************************/
/*
* @file   actor.cpp
* @author Aditya Harsh
* @brief  Actor class implementation.
*/
/******************************************************************************/

#include "actor.h"
#include "jobmanager.h"

namespace mifty
{
    /**
     * @brief Actor constructor
     * 
     * @param name 
     * @param active 
     */
    Actor::Actor(const std::string& name, bool active) noexcept : name_(name), active_(active), destroy_(false)
    {

    }

    /**
     * @brief Destructor for actors
     * 
     */
    Actor::~Actor()
    {
        
    }

    /**
     * @brief Updates each component within the actor
     * 
     */
    void Actor::Update()
    {
        // update each component
        for (auto& comp : components_)
        {
            // get the raw pointer
            auto c_ptr = comp.second.get();

            // update the active components
            if (c_ptr->Active())
                c_ptr->Update();
        }
    }

    /**
     * @brief Returns the active flag on the actor
     * 
     * @return true 
     * @return false 
     */
    bool Actor::Active() const noexcept
    {
        return active_;
    }

    /**
     * @brief Sets the active flag
     * 
     * @param active 
     */
    void Actor::SetActive(bool active) noexcept
    {
        active_ = active;
    }

    /**
     * @brief Returns the destroy flag on the actor
     * 
     * @return true 
     * @return false 
     */
    bool Actor::Destroy() const noexcept
    {
        return destroy_;
    }

    /**
     * @brief Sets the destroy flag
     * 
     * @param destroy 
     */
    void Actor::SetDestroy(bool destroy) noexcept
    {
        destroy_ = destroy;
    }

    /**
     * @brief Gets the name of the actor
     * 
     * @return const std::string& Actor::Name const 
     */
    const std::string& Actor::Name() const noexcept
    {
        return name_;
    }

    /**
     * @brief Sets the name of the actor
     * 
     * @param name 
     */
    void Actor::SetName(const std::string& name) noexcept
    {
        name_ = name;
    }
}