/******************************************************************************/
/*
* @file   gamemanager.cpp
* @author Aditya Harsh
* @brief  Manager is in charge of running the game.
*/
/******************************************************************************/

#include "gamemanager.h"
#include "actor.h"
#include "jobmanager.h"
#include "meshmanager.h"
#include "materialmanager.h"
#include "transform.h"
#include "render.h"
#include "mat_vertex_color.h"
#include "camera.h"
#include "graphics.h"
#include "light.h"
#include "playercontroller.h"
#include "physics.h"
#include "boxcollider.h"
#include "input.h"
#include "gametime.h"
#include "collisionmanager.h"
#include "optionparse.h"
#include "texturemanager.h"
#include "worldgen.h"

namespace mifty
{
    namespace
    {
        std::list<std::unique_ptr<Actor>> actor_list;
    }

    /**
     * @brief Initialzie the game manager
     * 
     */
    void GameManager::Init()
    {
        // load material
        MaterialManager::LoadMaterial(MaterialType::MAT_VERT_COL);
        // load cube
        MeshManager::LoadMesh(MeshID::CUBE);
        MeshManager::LoadMesh(MeshID::PLANE);

        TextureManager::LoadTexture("../Assets/Textures/dirt.jpg", TextureType::DIRT, true);
        TextureManager::LoadTexture("../Assets/Textures/pixel_grass.jpg", TextureType::GRASS, true);
        TextureManager::LoadTexture("../Assets/Textures/sand.png", TextureType::SAND, true);
        TextureManager::LoadTexture("../Assets/Textures/grass3.png", TextureType::COBBLE, true);
        TextureManager::LoadTexture("../Assets/Textures/block2.png", TextureType::GREEN, true);
        // create the actor
        std::unique_ptr<Actor> cam = std::make_unique<Actor>("Camera", true);
        cam.get()->AddComponent<Camera>(Graphics::GetWindowRatio());
        Camera* cam_ptr = cam.get()->GetComponent<Camera>();
        AddObject(cam);

        // create a light component
        std::unique_ptr<Actor> light = std::make_unique<Actor>("Light", true);
        light.get()->AddComponent<Light>(glm::vec3(0, 1000.0f, -300.0f), glm::vec3(1, 1.0f, 1));
        AddObject(light);

        // player component
        std::unique_ptr<Actor> player = std::make_unique<Actor>("Player", true);
        player.get()->AddComponent<Transform>(glm::vec3(0, 10.0f, 0.0f), glm::vec3(0.5f, 5.0f, 0.5f), glm::vec3(0, 0, 0));
        player.get()->AddComponent<Physics>(player.get()->GetComponent<Transform>(), false);
        //player.get()->AddComponent<BoxCollider>(player.get()->GetComponent<Physics>(), glm::vec3(0.5f, 3.0f, 0.5f), false);
        player.get()->AddComponent<PlayerController, 1>(cam_ptr, player.get()->GetComponent<Physics>());
        AddObject(player);
        
        // creates the world
        WorldGenerator::CreateWorld();
    }

    /**
     * @brief Update the game manager
     * 
     */
    void GameManager::Update()
    {
        for (auto it = std::begin(actor_list); it != std::end(actor_list); ++it)
        {
            // get the raw pointer
            auto a_ptr = (*it).get();

            // destroy actors flagged for removal
            if (a_ptr->Destroy())
            {
                actor_list.erase(it);
            }
            else if (a_ptr->Active()) // update the actor
            {
                a_ptr->Update();
            }
        }

        // check for collisions
        //JobManager::AddJob(CollisionManager::Resolve);
    }

    void GameManager::AddObject(std::unique_ptr<Actor>& actor)
    {
        actor_list.emplace_back(std::move(actor));
    }

    /**
     * @brief Returns the list of actors within the scene
     * 
     * @return std::list<std::unique_ptr<Actor>>& GameManager::GetList 
     */
    std::list<std::unique_ptr<Actor>>& GameManager::GetList() noexcept
    {
        return actor_list;
    }

    /**
     * @brief Clears out all of the actors
     * 
     */
    void GameManager::Clear()
    {
        actor_list.clear();
    }

    /**
     * @brief Shut down the game manager
     * 
     */
    void GameManager::ShutDown()
    {
        // clear all of the actors
        Clear();

        // remove all meshes
        MeshManager::RemoveAllMeshes();

        // remove all materials
        MaterialManager::RemoveAllMaterials();

        // remove all textures
        TextureManager::RemoveAllTextures();
    }
}