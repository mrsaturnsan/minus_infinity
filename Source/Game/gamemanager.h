/******************************************************************************/
/*
* @file   gamemanager.h
* @author Aditya Harsh
* @brief  Manager is in charge of running the game.
*/
/******************************************************************************/

#pragma once

#include "actor.h"

namespace mifty
{
    /**
     * @brief Game manager updates all actors existing within the game
     * 
     */
    class GameManager
    {
    public:
        // initialize the game manager
        static void Init();
        // update the game each frame
        static void Update();
        // free resources
        static void ShutDown();

        // adds an object
        static void AddObject(std::unique_ptr<Actor>& actor);
        // returns the object list
        static std::list<std::unique_ptr<Actor>>& GetList() noexcept;

        // functions for manipulating the list of actors
        static void Clear();
    private:
        // static class
        GameManager() = delete;
    };
}