/******************************************************************************/
/*
* @file   worldgen.cpp
* @author Aditya Harsh
* @brief  Generates the world.
*/
/******************************************************************************/

#include "worldgen.h"

#include "gamemanager.h"
#include "actor.h"
#include "meshmanager.h"
#include "materialmanager.h"
#include "transform.h"
#include "render.h"
#include "mat_vertex_color.h"
#include "physics.h"
#include "boxcollider.h"
#include "core.h"
#include "texturemanager.h"

namespace mifty
{
    /**
     * @brief Generates the world
     * 
     */
    void WorldGenerator::CreateWorld()
    {
        int range = 300;
        float size = 1.0f;
        float distance = 4 * size;

        int s = 0;

        int last_y = distance;

        for (int x = 0; x <= range; ++x)
        {
            for (int z = -range; z <= 0; ++z)
            {
                int y = last_y + distance * Core::GetRandomInt(-10, 10);
                //int y = Core::GetRandomInt(last_y - 1, last_y + 1);

                if (y < 0) y = 0;
                
                last_y = -y;
                //last_y = y;

                std::unique_ptr<Actor> rc = std::make_unique<Actor>("Cubes", true);
                rc.get()->AddComponent<Transform>(glm::vec3(x * distance, y, z * distance), glm::vec3(size, size, size), glm::vec3(0, 0, 0));

                s = Core::GetRandomInt(0, 4);
                if (s == 0)
                {
                    rc.get()->AddComponent<Render>(MeshID::CUBE, TextureType::DIRT, MaterialManager::GetMaterial(MaterialType::MAT_VERT_COL));
                }
                else if (s == 1)
                {
                    rc.get()->AddComponent<Render>(MeshID::CUBE, TextureType::GRASS, MaterialManager::GetMaterial(MaterialType::MAT_VERT_COL));
                }
                else if (s == 2)
                {
                    rc.get()->AddComponent<Render>(MeshID::CUBE, TextureType::SAND, MaterialManager::GetMaterial(MaterialType::MAT_VERT_COL));
                }
                else if (s == 3)
                {
                    rc.get()->AddComponent<Render>(MeshID::CUBE, TextureType::GREEN, MaterialManager::GetMaterial(MaterialType::MAT_VERT_COL));
                }
                else if (s == 4)
                {
                    rc.get()->AddComponent<Render>(MeshID::CUBE, TextureType::COBBLE, MaterialManager::GetMaterial(MaterialType::MAT_VERT_COL));
                }

                rc.get()->AddComponent<Physics>(rc.get()->GetComponent<Transform>(), true);
                //rc.get()->AddComponent<BoxCollider>(rc.get()->GetComponent<Physics>(), glm::vec3(size, size, size), true);
                GameManager::AddObject(rc);
            }
        }
    }
}