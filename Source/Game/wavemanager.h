/******************************************************************************/
/*
* @file   wavemanager.h
* @author Aditya Harsh
* @brief  Manages the spawning and despawning of waves and enemies.
*/
/******************************************************************************/

#pragma once

#include "actor.h"

namespace mifty
{
    /**
     * @brief Static class which manages enemies/waves
     * 
     */
    class WaveManager
    {
    public:
        // updates the wave manager each frame
        static void Update();
        // frees all memory being used by the wavemanger and enemies sitting around in memory
        static void ShutDown();
    private:
        // prevent instantiation of the class
        WaveManager() = delete;

        // private helper function to spawn an enemy
        static void SpawnEnemy(unsigned count);
    };
}