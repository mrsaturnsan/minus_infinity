/******************************************************************************/
/*
* @file   wavemanager.cpp
* @author Aditya Harsh
* @brief  Manages the spawning and despawning of waves and enemies.
*/
/******************************************************************************/

#include "wavemanager.h"
#include "utility.h"

namespace mifty
{

    /**
     * @brief Updates the wave manager each frame
     * 
     */
    void WaveManager::Update()
    {
        // do something fun
    }

    /**
     * @brief Shuts down the wave manager
     * 
     */
    void WaveManager::ShutDown()
    {
        // do something fun
    }

    /**
     * @brief Spawns as many enemies are requested
     * 
     * @param count 
     */
    void WaveManager::SpawnEnemy(unsigned count)
    {
        UNUSED(count);
    }
}