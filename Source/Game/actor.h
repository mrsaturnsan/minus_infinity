/******************************************************************************/
/*
* @file   actor.h
* @author Aditya Harsh
* @brief  Defines the actor class.
*/
/******************************************************************************/

#pragma once

#include "component.h"

namespace mifty
{
    class Actor
    {
    public:
        // constructor
        Actor(const std::string& name, bool active) noexcept;
        
        // destructor
        ~Actor();

        // disable copy and assignment
        Actor(const Actor&) = delete;
        Actor& operator=(const Actor&) = delete;

        // updates all of the components within the actor
        void Update();

        // adds a specified component
        template <typename T, unsigned priority = 0, typename ...Params>
        void AddComponent(Params&&... params);

        // gets a requested component
        template <typename T>
        T* GetComponent() noexcept;

        // gets active flag
        bool Active() const noexcept;
        // sets active flag
        void SetActive(bool active) noexcept;

        // gets destroy flag
        bool Destroy() const noexcept;
        // sets destroy flag
        void SetDestroy(bool destroy) noexcept;

        // gets the name
        const std::string& Name() const noexcept;
        // sets the name
        void SetName(const std::string& name) noexcept;

    private:
        // vector of all of the components attached to the actor
        std::vector<std::pair<std::type_index, std::unique_ptr<Component>>> components_;
        // name of the actor
        std::string name_;
        // active flag
        bool active_;
        // destroy flag
        bool destroy_;
    };

    // adds a specified component
    template <typename T, unsigned priority = 0, typename ...Params>
    void Actor::AddComponent(Params&&... params)
    {
        if (!GetComponent<T>())
        {
            // push back
            if (priority == 0 || components_.size() <= priority)
            {
                // emplace on the back automatically
                components_.emplace_back(std::type_index(typeid(T)), std::move(std::make_unique<T>(this, params...)));
            }
            else
            {
                // inserts element into the appropriate position within the vector
                components_.insert(std::begin(components_) + (priority - 1), std::make_pair(std::type_index(typeid(T)), std::move(std::make_unique<T>(this, params...))));
            }   
        }
        else
        {
           throw std::runtime_error("Attempting to add duplicate component!");
        }        
    }

    /**
     * @brief Gets a requested component
     * 
     * @tparam T 
     * @return T*
     */
    template <typename T>
    T* Actor::GetComponent() noexcept
    {
        const auto idx = std::type_index(typeid(T));
        const auto search_result = std::find_if(
        std::cbegin(components_), std::cend(components_),
        [=](const auto& e) { return idx == e.first; });
        return search_result == std::cend(components_) ? nullptr : static_cast<T*>(search_result->second.get()); 
    }
}