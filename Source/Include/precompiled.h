/******************************************************************************/
/*
* @file   precompiled.h
* @author Aditya Harsh
* @brief  Precompiled static files to reduce compilation time.
*/
/******************************************************************************/

#ifndef PRECOMPILED_H
#define PRECOMPILED_H

// macros
#define _DEBUG

// standard library
#include <iostream>
#include <sstream>
#include <fstream>
#include <assert.h>
#include <vector>
#include <list>
#include <map>
#include <string>
#include <chrono>
#include <memory>
#include <thread>
#include <atomic>
#include <cmath>
#include <functional>
#include <tuple>
#include <typeindex>
#include <mutex>
#include <condition_variable>
#include <cstring>
#include <random>

// os
#ifdef __APPLE__
    #include <signal.h> /* signal, signal defines */
    #include <getopt.h>
#endif

// OpenGL
#define GLEW_STATIC
#include <GL/glew.h>

#ifdef __APPLE__
    #include <OpenGL/gl.h>
#else
#ifdef _WIN32
    #include <GL/gl.h>
#endif
#endif

// GLFW
#include <GLFW/glfw3.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/constants.hpp>

// SOIL2
#include "../../Lib/Soil/SOIL2.h"

// chaiscript
//#include <chaiscript/chaiscript.hpp>

#endif
