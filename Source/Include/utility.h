/******************************************************************************/
/*
* @file   utility.h
* @author Aditya Harsh
* @brief  Contains useful macros for the application.
*/
/******************************************************************************/

#pragma once

// Gets rid of compiler warnings for unused variables
#define UNUSED(x) (void)(x)