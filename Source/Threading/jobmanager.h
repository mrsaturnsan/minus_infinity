/******************************************************************************/
/*
* @file   jobmanager.h
* @author Aditya Harsh
* @brief  Class which deals with all available jobs
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    typedef std::function<void (void)> Job;

    class JobManager
    {
    public:
        // adds a job for the main thread
        static void AddJobMain(Job new_job);
        // adds a job to the queue
        static void AddJob(Job new_job);
        // return work status
        static bool Working();
        // runs tasks within the blocked queue
        static void RunTasks();
        // shuts down each blocked thread
        static void ShutDown();
    private:
        // prevent instantiation
        JobManager() = delete;       
    };
}