/******************************************************************************/
/*
* @file   threadmanager.h
* @author Aditya Harsh
* @brief  Manages the thread class.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    class ThreadManager
    {
    public:
        static void InitThreads() noexcept;
        static void Join() noexcept;
    private:
        ThreadManager() = delete;
    };
}