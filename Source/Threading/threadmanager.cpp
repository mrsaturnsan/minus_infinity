/******************************************************************************/
/*
* @file   threadmanager.cpp
* @author Aditya Harsh
* @brief  Manages the thread class.
*/
/******************************************************************************/

#include "threadmanager.h"
#include "core.h"
#include "jobmanager.h"

namespace mifty
{
    namespace
    {
        std::vector<std::thread> threads;
    }

    /**
     * @brief Creates all of the threads
     * 
     */
    void ThreadManager::InitThreads() noexcept
    {
        // account for main thread
        unsigned thread_count = Core::GetMaxThreads() - 1;

        try
        {
            // start running tasks
            for (unsigned i = 0; i < thread_count; ++i)
                threads.emplace_back(std::thread(JobManager::RunTasks));
                 
        }
        catch (...) {}
    }

    /**
     * @brief Join all of the threads
     * 
     */
    void ThreadManager::Join() noexcept
    {
        try
        {
            for (auto& i : threads)
                i.join();
        }
        catch (...) {}
    }
}
