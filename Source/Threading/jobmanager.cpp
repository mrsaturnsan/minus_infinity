/******************************************************************************/
/*
* @file   jobmanager.cpp
* @author Aditya Harsh
* @brief  Class which deals with all available jobs
*/
/******************************************************************************/

#include "jobmanager.h"
#include "blockingconcurrentqueue.h"
#include "core.h"
#include "gametime.h"

namespace mifty
{
    namespace
    {
        // the queue of jobs
        moodycamel::BlockingConcurrentQueue<Job> job_queue;

        // jobs for the main queue to perform
        moodycamel::ConcurrentQueue<Job> job_queue_main;

        // mutex to ensure jobs are executed sequentially
        std::mutex job_lock;

        // whether or not the threads should be running jobs
        bool should_run_jobs = true;
    }

    // adds a job for the main thread
    void JobManager::AddJobMain(Job new_job)
    {
        job_queue_main.enqueue(new_job);
    }

    /**
     * @brief Adds a new job to the queue
     * 
     * @param new_job Function pointer
     */
    void JobManager::AddJob(Job new_job)
    {
        job_queue.enqueue(new_job);
    }

    /**
     * @brief Execute queued up tasks
     * 
     */
    void JobManager::RunTasks()
    {
        // job to be run
        Job job;

        if (std::this_thread::get_id() == Core::GetMainThreadID())
        {
            // main thread should jump out as soon as there are no jobs left
            while (job_queue_main.try_dequeue(job))
                job();
        }
        else
        {
            while (should_run_jobs)
            {
                // wait for jobs to come in, and deque
                job_queue.wait_dequeue(job);
                
                // call functions
                job();
            }
        }
    }

    /**
     * @brief Whether or not something is in progress
     * 
     * @return true 
     * @return false 
     */
    bool JobManager::Working()
    {
        return job_queue.size_approx();
    }

    /**
     * @brief Shuts down each thread
     * 
     */
    void JobManager::ShutDown()
    {
        // shut down
        should_run_jobs = false;

        // account for main thread
        unsigned thread_count = Core::GetMaxThreads() - 1;

        // send bogus functions to release the threads
        for (unsigned i = 0; i < thread_count; ++i)
            AddJob([]{});
    }
}