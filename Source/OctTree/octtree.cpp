/******************************************************************************/
/*
* @file   octtree.cpp
* @author Aditya Harsh
* @brief  Responsible for spatial partioning
*/
/******************************************************************************/

#include "octtree.h"
#include "boxcollider.h"
#include "physics.h"
#include "actor.h"

namespace mifty
{
    /**
     * @brief Constructor
     * 
     * @param level 
     * @param minx 
     * @param maxx 
     * @param miny 
     * @param maxy 
     * @param minz 
     * @param maxz 
     */
    OctTree::OctTree(unsigned level, const Bounds& bounds) :
        level_(level), bounds_(bounds)
    {
        colliders_.reserve(MAX_OBJECTS + 1);
    }

    /**
     * @brief Destructor
     * 
     */
    OctTree::~OctTree()
    {

    }

    /**
     * @brief Clears the tree
     * 
     */
    void OctTree::Clear()
    {
        // iterate over all the children and delete them
        for (auto& i : nodes_)
        {
            if (i)
            {
                i->Clear();
                delete i;
                i = nullptr;
            }
        }
    }

    /**
     * @brief Removes a collider from the quad tree
     * 
     * @param collider 
     */
    void OctTree::Remove(const BoxCollider* collider)
    {
        // get the index
        int index = GetIndex(collider);

        // get the object
        if (index != -1 && nodes_[0])
            nodes_[index]->Remove(collider);

        for (unsigned i = 0; i < colliders_.size(); ++i)
        {
            if (colliders_[i] == collider)
            {
                colliders_.erase(std::begin(colliders_) + i);
            }
        }
    }

    /**
     * @brief Balances tree
     * 
     */
    void OctTree::Balance(std::list<BoxCollider*>& list)
    {
        // check if this node has child nodes
        if (nodes_.front())
        {
            // check if parent colliders can be redistributed
            if (!colliders_.empty())
            {
                ChildFitInsert();
            }
                
            // check child nodes
            for (auto&i : nodes_)
                i->Balance(list);

            // remove useless nodes (no colliders nor children)
            for (auto& i : nodes_)
                if (!i->colliders_.empty() || i->nodes_.front())
                    return;

            // clear all of the nodes
            Clear();
        }
        else if (!colliders_.empty()) // no nodes + has colliders (bottom of the tree)
        {
            unsigned i = 0;

            while (i < colliders_.size())
            {
                int index = GetIndex(colliders_[i], 0.5f);

                // -1 = no fit
                if (index == -1)
                {
                    list.emplace_back(colliders_[i]);
                    colliders_.erase(std::begin(colliders_) + i);
                }
                else 
                {
                    ++i;
                }
            }

            // deal with extra colliders that may have been inserted
            //&& level_ < MAX_LEVELS
            if (colliders_.size() > MAX_OBJECTS)
            {
                Split();
                ChildFitInsert();
            }
        }
    }

    /**
     * @brief Inserts a collider into the tree
     * 
     * @param collider 
     */
    void OctTree::Insert(BoxCollider* collider)
    {
        if (!collider) 
            throw std::runtime_error("Attempting to insert a bad collider into tree");

        // check if nodes are allocated
        if (nodes_.front())
        {
            int index = GetIndex(collider);

            if (index != -1)
            {
                nodes_[index]->Insert(collider);
                return;
            }
        }

        // add the collider
        colliders_.emplace_back(collider);

        //  && level_ < MAX_LEVELS
        if (colliders_.size() > MAX_OBJECTS)
        {
            // check if nodes have been alloacted
            if (!nodes_.front())
                Split();

            ChildFitInsert();
        }
    }

    /**
     * @brief Returns the list of possible colliders
     * 
     * @param p_list 
     * @param collider 
     */
    void OctTree::Retrieve(objects& p_list, const BoxCollider* collider)
    {
        // get the index
        int index = GetIndex(collider);

        // get the object
        if (index != -1 && nodes_[0])
        {
            nodes_[index]->Retrieve(p_list, collider);
        }
            
            
        std::for_each(std::begin(colliders_), std::end(colliders_), 
        [&p_list, collider](BoxCollider* col)
        {
            if (collider != col)
                p_list.emplace_back(col);
        });
    }

    /**
     * @brief Retrieves a vector of pointers to all of the colliders
     * 
     * @param vec 
     */
    void OctTree::RetrieveAll(objects& vec)
    {
        // create a vector if colliders exist
        if (!colliders_.empty())
        {
            std::for_each(std::begin(colliders_), std::end(colliders_),
            [&vec](BoxCollider* box)
            {
                if (box)
                {
                    vec.emplace_back(box);
                }
            });
        }

        // fill in with children colliders
        for (auto& i : nodes_)
        {
            // check if children exist
            if (!i) break;

            // fill in with children colliders
            i->RetrieveAll(vec);
        }
    }

    /**
     * @brief Splits the tree
     * 
     */
    void OctTree::Split()
    {
        Bounds node_bound[] = 
        {
            // top - back - right
            Bounds
            {
                bounds_.width_ / 2.0f,
                bounds_.maxx_,
                bounds_.height_ / 2.0f,
                bounds_.maxy_,
                bounds_.minz_,
                bounds_.depth_ / 2.0f
            },

            // top - back - left
            Bounds
            {
                bounds_.minx_,
                bounds_.width_ / 2.0f,
                bounds_.height_ / 2.0f,
                bounds_.maxy_,
                bounds_.minz_,
                bounds_.depth_ / 2.0f
            },

            // top - front - left
            Bounds
            {
                bounds_.minx_,
                bounds_.width_ / 2.0f,
                bounds_.height_ / 2.0f,
                bounds_.maxy_,
                bounds_.depth_ / 2.0f,
                bounds_.maxz_
            },

            // top - front - right
            Bounds
            {
                bounds_.width_ / 2.0f,
                bounds_.maxx_,
                bounds_.height_ / 2.0f,
                bounds_.maxy_,
                bounds_.depth_ / 2.0f,
                bounds_.maxz_
            },

            // bottom - back - right
            Bounds
            {
                bounds_.width_ / 2.0f,
                bounds_.maxx_,
                bounds_.miny_,
                bounds_.height_ / 2.0f,
                bounds_.minz_,
                bounds_.depth_ / 2.0f
            },

            // bottom - back - left
            Bounds
            {
                bounds_.minx_,
                bounds_.width_ / 2.0f,
                bounds_.miny_,
                bounds_.height_ / 2.0f,
                bounds_.minz_,
                bounds_.depth_ / 2.0f
            },

            // bottom - front - left
            Bounds
            {
                bounds_.minx_,
                bounds_.width_ / 2.0f,
                bounds_.miny_,
                bounds_.height_ / 2.0f,
                bounds_.depth_ / 2.0f,
                bounds_.maxz_
            },

            // bottom - front - right
            Bounds
            {
                bounds_.width_ / 2.0f,
                bounds_.maxx_,
                bounds_.miny_,
                bounds_.height_ / 2.0f,
                bounds_.depth_ / 2.0f,
                bounds_.maxz_
            }
        };

        // set the level
        const unsigned new_level = level_ + 1;

        // create the nodes with the appropriate sizes
        for (unsigned i = 0; i < 8; ++i)
        {
            if (nodes_[i])
                throw std::runtime_error("Should not be allocated");
                
            // allocate the node
            nodes_[i] = new OctTree(new_level, node_bound[i]);
        }
    }

    /**
     * @brief Figures out which node the object should be in
     * 
     * @param collider 
     * @param divisor
     * @return int OctTree::GetIndex 
     */
    int OctTree::GetIndex(const BoxCollider* collider, float divisor) const
    {
        if (!collider) throw std::runtime_error("Invalid collider index!");
        if (divisor == 0) throw std::runtime_error("Attempting to divide by zero!");

        // the quadrant the object is in (-1 = can't be fit into one box)
        int index = -1;

        // get the mid points for all dimensions
        float x_mid = bounds_.width_  / divisor;
        float y_mid = bounds_.height_ / divisor;
        float z_mid = bounds_.depth_  / divisor;

        // figure out if the collider fits
        bool top_quad    =  collider->miny_ > y_mid;
        bool bottom_quad =  collider->miny_ < y_mid && collider->maxy_ < y_mid;
        bool front_quad  =  collider->minz_ > z_mid;
        bool back_quad   =  collider->minz_ < z_mid && collider->maxz_ < z_mid;
        bool left_quad   =  collider->minx_ < x_mid && collider->maxx_ < x_mid;
        bool right_quad  =  collider->minx_ > x_mid;

        // top - back - right
        if (top_quad && back_quad && right_quad)
            index = 0;

        // top - back - left
        if (top_quad && back_quad && left_quad)
        {
            if (index != -1)
                return -1;
            index = 1;
        }
            

        // top - front - left
        if (top_quad && front_quad && left_quad)
        {
            if (index != -1)
                return -1;
            index = 2;
        }

        // top - front - right
        if (top_quad && front_quad && right_quad)
        {
            if (index != -1)
                return -1;
            index = 3;
        }

        // bottom - back - right
        if (bottom_quad && back_quad && right_quad)
        {
            if (index != -1)
                return -1;
            index = 4;
        }

        // bottom - back - left
        if (bottom_quad && back_quad && left_quad)
        {
            if (index != -1)
                return -1;
            index = 5;
        }

        // bottom - front - left
        if (bottom_quad && front_quad && left_quad)
        {
            if (index != -1)
                return -1;
            index = 6;
        }

        // bottom - front - right
        if (bottom_quad && front_quad && right_quad)
        {
            if (index != -1)
                return -1;
            index = 7;
        }

        return index;
    }

    /**
     * @brief Gets the collider
     * 
     * @param collider 
     * @return int 
     */
    int OctTree::GetIndexOne(const BoxCollider* collider)
    {
        if (!collider) throw std::runtime_error("Invalid collider");

        // get the index
        int index = GetIndex(collider);

        if (nodes_.front() && index >= 0)
        {
            index = nodes_[index]->GetIndexOne(collider);
        }

        return index;
    }

    /**
     * @brief Inserts into child
     * 
     */
    void OctTree::ChildFitInsert()
    {
        unsigned i = 0;

        while (i < colliders_.size())
        {
            int index = GetIndex(colliders_[i]);

            if (index != -1)
            {
                BoxCollider* temp = colliders_[i];
                colliders_.erase(std::begin(colliders_) + i);
                nodes_[index]->Insert(temp);
            }
            else 
            {
                ++i;
            }
        }
    }
}