/******************************************************************************/
/*
* @file   octtree.h
* @author Aditya Harsh
* @brief  Responsible for spatial partioning
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    // forward declare
    class BoxCollider;
    class Actor;

    /**
     * @brief Represents a bounded area in 3D space
     * 
     */
    struct Bounds
    {
        /**
         * @brief Container
         * 
         */
        Bounds(float minx, float maxx, float miny, float maxy, float minz, float maxz) :
            minx_(minx), maxx_(maxx), miny_(miny), maxy_(maxy), minz_(minz), maxz_(maxz), 
            width_(maxx + minx), height_(maxy + miny), depth_(maxz + minz) {}

        // data regarding positions
        float minx_;
        float maxx_;
        float miny_;
        float maxy_;
        float minz_;
        float maxz_;
        
        // data about lengths
        float width_;
        float height_;
        float depth_;
    };

    // define types
    using objects = std::vector<BoxCollider*>;

    /**
     * @brief Collision optimization
     * 
     */
    class OctTree
    {
    public: 
        OctTree(unsigned level, const Bounds& bounds);
        ~OctTree();
        // inserts into tree
        void Insert(BoxCollider* collider);
        // recursively clears the oct-tree
        void Clear();
        // removes a collider from the quad tree
        void Remove(const BoxCollider* collider);
        // balances the tree
        void Balance(std::list<BoxCollider*>& list);
        // retrieves vector of possible objects that can be colliding
        void Retrieve(std::vector<BoxCollider*>& p_list, const BoxCollider* collider);
        // retrieves objects of all indexes
        void RetrieveAll(objects& vec);
        // gets the index of a collider
        int GetIndexOne(const BoxCollider* collider);
    private:
        // defines the limits of the tree
        static const unsigned MAX_OBJECTS = 8;
        //static const unsigned MAX_LEVELS = 1000;

        // define types
        using node = std::array<OctTree*, MAX_OBJECTS>;

        // splits the tree
        void Split();
        // gets the index a collider should get put in
        int GetIndex(const BoxCollider* collider, float divisor = 2.0f) const;
        // balances and inserts into child
        void ChildFitInsert();

        // the current level of the node
        unsigned level_;
        // the bounds of the node
        Bounds bounds_;
        // the colliders being checked
        objects colliders_;
        // the nodes around the point
        node nodes_ = {nullptr};
    };
}