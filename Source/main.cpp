/******************************************************************************/
/*
* @file   main.cpp
* @author Aditya Harsh
* @brief  Enter application
*/
/******************************************************************************/

#include "optionparse.h"
#include "game.h"

using namespace mifty;

/**
 * @brief Main function
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char** argv)
{
    // parse command line arguments
    OptionParse::Parse(argc, argv);

    // start the game
    Game::Boot();

    // clean exit
    return 0;
}
