/******************************************************************************/
/*
* @file   collisionmanager.h
* @author Aditya Harsh
* @brief  Responsible for managing collisions
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    // forward declare
    class BoxCollider;

    /**
     * @brief Class for checking collisions between objects
     * 
     */
    class CollisionManager
    {
    public:
        // register self for collision checking
        static void Register(BoxCollider* collider);
        // resolve collisions
        static void Resolve();
        // clear collider list
        static void Clear();
    private:
        // prevent instantiations
        CollisionManager() = delete;
        // balances collision tree
        static void Balance();
    };
}