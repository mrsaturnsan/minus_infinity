/******************************************************************************/
/*
* @file   transform.h
* @author Aditya Harsh
* @brief  Transforms allow for movement, rotation, and scaling.
*/
/******************************************************************************/

#pragma once

#include "component.h"

namespace mifty
{
    // forward declare
    class Actor;

    class Transform : public Component
    {
    public:
        // constructor
        Transform(Actor* parent, const glm::vec3& position, const glm::vec3& scale, const glm::vec3& rotation);
        ~Transform() noexcept;

        // modify the position on the transform
        void SetPosition(const glm::vec3& position) noexcept;
        void AddPosition(const glm::vec3& position) noexcept;
        const glm::vec3& GetPosition() const noexcept;

        // modify the scale on the transform
        void SetScale(const glm::vec3& scale) noexcept;
        void AddScale(const glm::vec3& scale) noexcept;
        const glm::vec3& GetScale() const noexcept;

        // modify the rotation on the transform
        void SetRotation(const glm::vec3& rotation) noexcept;
        void AddRotation(const glm::vec3& rotation) noexcept;
        const glm::vec3& GetRotation() const noexcept;

        // gets the main matrix
        const glm::mat4& GetMatrix() const noexcept;

        // pure virtual
        virtual void Update() override;
        // pure virtual
        virtual std::unique_ptr<Component> Clone() override;

        // force a recalculation
        void ForceRecalculate();

    private:
        // Recalculate the transform matrix if flagged
        void RecalculateMatrix() noexcept;

        // stores position, rotation, and scale
        glm::mat4 matrix_;

        // store individually to avoid decomposing many times
        glm::vec3 translation_;
        glm::vec3 scale_;
        glm::vec3 rotation_;

        // recalculate matrix flag
        bool is_dirty_;
    };
}