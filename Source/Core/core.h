/******************************************************************************/
/*
* @file   core.h
* @author Aditya Harsh
* @brief  Responsible for core data
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    class Core
    {
    public:
        static void InitCore() noexcept;
        // gets the maximum number of concurrent threads supported by the hardware
        static unsigned int GetMaxThreads() noexcept;
        // gets the id of the main thread
        static std::thread::id GetMainThreadID() noexcept;
        // generates a random int
        static int GetRandomInt(int min, int max) noexcept;
        // returns a random float
        float GetRandomFloat(float min, float max, unsigned int precision) noexcept;
    private:
        Core() = delete;

        // checks supported threats
        static void CheckConcurrentThreads() noexcept;
        // installs signal handlers
        static void InstallSignalHandler() noexcept;
        // initializes all of the resource managers
        static void InitResCache() noexcept;
        // init random number generator
        static void InitRandom() noexcept;
    };
}