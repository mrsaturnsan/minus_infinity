/******************************************************************************/
/*
* @file   optionparse.cpp
* @author Aditya Harsh
* @brief  Parse provided options.
*/
/******************************************************************************/

#include "optionparse.h"

namespace mifty
{
    namespace
    {
        // static variables
        bool lockmouse = true;
        int objects = 5;
    }  
    
    /**
    * @brief Parses options given to the command line
    * 
    * @param argc Number of arguments
    * @param argv Pointer to the arguments
    */
    void OptionParse::Parse(int argc, char** argv) noexcept
    {
        while (1) 
        {
            int option_index = 0;

            static struct option long_options[] = 
            {
                {"lockmouse",      required_argument,       NULL, 'l'},
                {"objects",        required_argument,       NULL, 'o'},
                {NULL,                             0,       NULL, 0}
            };
        
            int c = getopt_long(argc, argv, ":l:o:", long_options, &option_index);           

            if (c == -1)
                break;
        
            switch (c) 
            {
            case 'l':
                    if (std::strcmp(optarg, "false") == 0)
                    {
                        lockmouse = false;
                    }
                    else if (std::strcmp(optarg, "true") == 0)
                    {
                        lockmouse = true;
                    }
                break;

            case 'o':
                    objects = std::stoi(optarg);
                break;

            case '?':
                    std::cout << "Unknown argument\n";
                break;
        
            case ':':
                    std::cout << "Missing argument\n";
            }
        }
        // if (optind < argc)
        // {
        //     // argv[optind] will be the language (english or 日本語)
        // }
    }

    /**
     * @brief Returns whether or not to lock the mouse
     * 
     * @return true 
     * @return false 
     */
    bool OptionParse::LockMouse() noexcept
    {
        return lockmouse;
    }

    /**
     * @brief Gets number of objects
     * 
     * @return int OptionParse::GetObjects 
     */
    int OptionParse::GetObjects() noexcept
    {
        return objects;
    }
}
