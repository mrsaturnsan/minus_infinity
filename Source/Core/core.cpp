/******************************************************************************/
/*
* @file   core.cpp
* @author Aditya Harsh
* @brief  Responsible for core data (CPU speed, memory, cores, cache, etc)
*/
/******************************************************************************/

#include "core.h"

namespace mifty
{
    namespace
    {
        // maximum number of supported threads
        unsigned int max_threads;

        // the id of the main thread
        std::thread::id main_thread_id;

        std::unique_ptr<std::mt19937> generator;
    }
    
    // handles signals from the OS
    static void SignalHandler(int sig) noexcept;

    /**
     * @brief Initializes core information
     * 
     */
    void Core::InitCore() noexcept
    {
        CheckConcurrentThreads();
        InstallSignalHandler();
        InitResCache();
        InitRandom();
    }

    /**
     * @brief Checks number of threads that can run together
     * 
     */
    void Core::CheckConcurrentThreads() noexcept
    {
        max_threads = std::thread::hardware_concurrency();
        main_thread_id = std::this_thread::get_id();
    }

    /**
     * @brief Installs signal handlers
     * 
     */
    void Core::InstallSignalHandler() noexcept
    {
        // struct of signal handling data
        struct sigaction act;

        // set values
        sigemptyset(&act.sa_mask);
        act.sa_flags = 0;
        act.sa_handler = SignalHandler;

        // install signal handling
        assert(!(sigaction(SIGINT, &act, NULL) == -1) && 
               "Could not install signal handler");
        assert(!(sigaction(SIGQUIT, &act, NULL) == -1) && 
               "Could not install signal handler");
        assert(!(sigaction(SIGUSR1, &act, NULL) == -1) && 
               "Could not install signal handler");
        assert(!(sigaction(SIGUSR2, &act, NULL) == -1) && 
               "Could not install signal handler");
    }

    /**
     * @brief Intercepts signals from the operating system
     * 
     */
    static void SignalHandler(int sig) noexcept
    {
        // TODO: Send Quit message
        std::cout << "OS signal " << sig << " handled\n";
    }

    /**
     * @brief Initializes the resource caches for each separate system
     * 
     */
    void Core::InitResCache() noexcept
    {

    }

    /**
     * @brief Returns the maximum number of threads supported
     * 
     * @return unsigned Core::GetMaxThreads 
     */
    unsigned int Core::GetMaxThreads() noexcept
    {
        return max_threads;
    }

    /**
     * @brief Retrieve the ID of the main thread
     * 
     * @return std::thread::id the id
     */
    std::thread::id Core::GetMainThreadID() noexcept
    {
        return main_thread_id;
    }

    /**
     * @brief Initializes random number generation.
     * 
     */
    void Core::InitRandom() noexcept
    {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        generator = std::make_unique<std::mt19937>(seed);
    }

    /**
     * @brief Generates an int.
     * 
     * @param min 
     * @param max 
     * @return int Core::GetRandomInt 
     */
    int Core::GetRandomInt(int min, int max) noexcept
    {
        return (*generator.get())() % (max - min + 1) + min;
    }

    /**
     * @brief Generates a float.
     * 
     * @param min 
     * @param max 
     * @param precision 
     * @return float Core::GetRandomFloat 
     */
    float Core::GetRandomFloat(float min, float max, unsigned int precision) noexcept
    {
        float result = (((float)(*generator.get())() / (float)generator.get()->max()) * (max - min)) + min;

        return (float)round(result * (precision * 10)) / (precision * 10);
    }
}