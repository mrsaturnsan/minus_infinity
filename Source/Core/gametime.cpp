/******************************************************************************/
/*
* @file   gametime.cpp
* @author Aditya Harsh
* @brief  Manages time related data.
*/
/******************************************************************************/

#include "gametime.h"

namespace mifty
{
    // stores the initial starting time
    namespace
    {
        // initial time
        auto time_init = std::chrono::steady_clock::now();
        // the current time
        auto current_time = std::chrono::steady_clock::now();
        // the last time
        auto last_time = std::chrono::steady_clock::now();
        // the amount of milliseconds since the last frame
        float delta_time;
        // fixed delta time for physics update
        const float fixed_delta_time = 0.02f;

        float constexpr MinDT(unsigned fps)
        {
            return 1.0f / static_cast<float>(fps);
        }
    }  

    /**
     * @brief Tracks how many milliseconds has passed since the application started
     * 
     * @return Milliseconds
     */
    unsigned long GameTime::GetTimeSinceStart() noexcept
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>
            (std::chrono::steady_clock::now() - time_init).count();    
    }

    float GameTime::GetDeltaTime() noexcept
    {
        return delta_time;
    }

    float GameTime::GetFixedDeltaTime() noexcept
    {
        return fixed_delta_time;
    }

    void GameTime::UpdateDeltaTime() noexcept
    {
        last_time = current_time;
        current_time = std::chrono::steady_clock::now();
        delta_time = std::chrono::duration_cast<std::chrono::duration<float>>
            (current_time - last_time).count();   
    }

    /**
     * @brief Imposes a limit on the frame rate to not run at max CPU capacity.
     * 
     */
    void GameTime::FrameLimit() noexcept
    {
        if (delta_time < MinDT(60))
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<unsigned>(MinDT(60) * 1000 - delta_time * 1000)));
        }
    }
}