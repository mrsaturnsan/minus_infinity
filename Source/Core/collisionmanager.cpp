/******************************************************************************/
/*
* @file   collisionmanager.cpp
* @author Aditya Harsh
* @brief  Responsible for managing collisions
*/
/******************************************************************************/

#include "collisionmanager.h"
#include "boxcollider.h"
#include "physics.h"
#include "transform.h"
#include "jobmanager.h"
#include "octtree.h"
#include "actor.h"

#define SPACE 100

namespace mifty
{
    namespace
    {
        OctTree tree(0, Bounds(-SPACE, SPACE, -SPACE, SPACE, -SPACE, SPACE));
    }

    /**
     * @brief Call to register self for collision checking
     * 
     * @param collider 
     */
    void CollisionManager::Register(BoxCollider* collider)
    {
        if (collider)
        {
            tree.Insert(collider);
        } 
        else
        {
            throw std::runtime_error("Corrupt collider passed in");
        }
    }

    /**
     * @brief Resolves all collisions between objects
     * 
     */
    void CollisionManager::Resolve()
    {
        // account for changes the occured to the colliders 
        Balance();

        // retrieve potential collisions
        objects potential;
        tree.RetrieveAll(potential);
        
        // don't process if empty
        if (potential.empty())
            return;

        // store the size
        const unsigned potential_size = potential.size();

        for (unsigned i = 0; i < potential_size; ++i)
        {
            // get objects near me
            objects near;

            tree.Retrieve(near, potential[i]);

            // get out if empty
            if (near.empty())
                continue;

            const unsigned t_size = near.size();

            for (unsigned j = 0; j < t_size; ++j)
            {
                if (potential[i]->CheckCollision(near[j]))
                {
                    potential[i]->GetPhysics()->ResolveCollision(potential[i], near[j]);
                }
            }
        }
    }

    /**
     * @brief Balances the collision tree
     * 
     */
    void CollisionManager::Balance()
    {
        // balance the tree
        std::list<BoxCollider*> list;

        tree.Balance(list);

        std::for_each(std::begin(list), std::end(list),
        [](BoxCollider* box)
        {
            tree.Insert(box);
        });
    }

    /**
     * @brief Clears the oct-tree
     * 
     */
    void CollisionManager::Clear()
    {
        // clears the tree
        tree.Clear();
    }
}