/******************************************************************************/
/*
* @file   materialmanager.h
* @author Aditya Harsh
* @brief  Manages the materials within the scene
*/
/******************************************************************************/

#pragma once

#include "material.h"

namespace mifty
{
    enum class MaterialType
    {
        MAT_VERT_COL
    };

    class MaterialManager
    {
    public:
        // loads a given material into memory
        static void LoadMaterial(MaterialType type);
        // removes a given material from memory
        static void RemoveMaterial(MaterialType type);
        // draws all of the materials within the scene
        static void DrawMaterials();
        // gets a material
        static Material& GetMaterial(MaterialType type);
        // removes all materials from memory
        static void RemoveAllMaterials();
    private:
        // prevent instantiation of the class
        MaterialManager() = delete;
    };
}