/******************************************************************************/
/*
* @file   game.h
* @author Aditya Harsh
* @brief  Contains core game startup, run, and exit logic.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    /**
     * @brief Class containing core engine logic
     * 
     */
    class Game
    {
    public:
        // Start the process
        static void Boot() noexcept;

    private:
        // prevent instantiation (static class)
        Game() = delete;

        static void Init();
        static void Run();
        static void ShutDown();
    };
}
