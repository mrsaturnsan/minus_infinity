/******************************************************************************/
/*
* @file   input.cpp
* @author Aditya Harsh
* @brief  Handles game input
*/
/******************************************************************************/

#include "input.h"
#include "utility.h"
#include "camera.h"

namespace mifty
{
    namespace
    {
        // maximum number of keys supported
        const int MAX_KEYS = 1024;
        // position of the mouse
        glm::vec2 mouse_pos;
        // used for keeping track of which keys are pressed
        bool input_key[MAX_KEYS];
        // used for trigger input
        bool triggered_keys[MAX_KEYS];
        // first mouse call check
        bool firstMouse = true;
    }

    /**
     * @brief Checks whether or not a key is valid
     * 
     * @param key 
     * @return true 
     * @return false 
     */
    bool Input::KeyInRange(int key)
    {
        if (key >= 0 && key < MAX_KEYS)
            return true;
        return false;
    }

    /**
     * @brief Callback for whenever a key is pressed
     * 
     * @param window 
     * @param key 
     * @param scancode 
     * @param action 
     * @param mode 
     */
    void Input::InputCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
    {
        UNUSED(window);
        UNUSED(scancode);
        UNUSED(mode);

        if (KeyInRange(key))
        {
            if (action == GLFW_PRESS)
            {
                input_key[key] = true;
            }
            else if (action == GLFW_RELEASE)
            {
                input_key[key] = false;
            }
        }
    }

    /**
     * @brief Callback for mouse scrolling
     * 
     * @param window 
     * @param xoffset 
     * @param yoffset 
     */
    void Input::ScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
    {
        UNUSED(window);
        UNUSED(xoffset);
        UNUSED(yoffset);
    }

    /**
     * @brief Callback for mouse movement
     * 
     * @param window 
     * @param xpos 
     * @param ypos 
     */
    void Input::MouseCallBack(GLFWwindow* window, double xpos, double ypos)
    {
        UNUSED(window);

        // check if this is the first mouse call
        if (firstMouse)
        {
            mouse_pos.x = xpos;
            mouse_pos.y = ypos;
            firstMouse = false;
        }

        // calcualte x and y offsets
        float xoffset = xpos - mouse_pos.x;
        float yoffset = mouse_pos.y - ypos;

        // set the mouse position
        mouse_pos.x = xpos;
        mouse_pos.y = ypos;

        // update camera rotation
        Camera* cam = Camera::GetActiveCamera();
        if (cam)
            cam->ProcessMouseMovement(xoffset, yoffset, true);
    }

    /**
     * @brief Checks for currently pressed key
     * 
     * @param key 
     * @return true 
     * @return false 
     */
    bool Input::InputCurrent(int key)
    {
        if (KeyInRange(key))
        {
            return input_key[key];
        }
        return false;
    }

    /**
     * @brief Checks for triggered key
     * 
     * @param key 
     * @return true 
     * @return false 
     */
    bool Input::InputTriggered(int key)
    {
        if (KeyInRange(key))
        {
            if (input_key[key] && !triggered_keys[key])
            {
                triggered_keys[key] = true;
                return true;
            }
            else if (!input_key[key] && triggered_keys[key])
            {
                triggered_keys[key] = false;
            }
        }
        return false;
    }

    /**
     * @brief Checks for mouse button callback
     * 
     * @param window 
     * @param button 
     * @param action 
     * @param m 
     */
    void Input::MouseButtonCallBack(GLFWwindow* window, int button, int action, int m)
    {
        InputCallback(window, button, 0, action, m);
    }

    /**
     * @brief Gets NDC mouse position
     * 
     * @return const glm::vec2&
     */
    const glm::vec2& Input::GetMousePosition()
    {
        return mouse_pos;
    }
}
