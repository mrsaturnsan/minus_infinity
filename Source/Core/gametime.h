/******************************************************************************/
/*
* @file   gametime.h
* @author Aditya Harsh
* @brief  Manages time related information.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    class GameTime
    {
    public:
        static unsigned long GetTimeSinceStart() noexcept;
        static float GetDeltaTime() noexcept;
        static float GetFixedDeltaTime() noexcept;
        static void UpdateDeltaTime() noexcept;
        static void FrameLimit() noexcept;
    private:
        // make singleton class
        GameTime() = delete;
    };
}