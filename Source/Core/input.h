/******************************************************************************/
/*
* @file   input.h
* @author Aditya Harsh
* @brief  Handles game input
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    /**
     * @brief General purpose input class
     * 
     */
    class Input
    {
    public:
        // callback for typing
        static void InputCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
        // callback for scrolling
        static void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
        // callback for mouse movement
        static void MouseCallBack(GLFWwindow* window, double xpos, double ypos);
        // callback for clicks
        static void MouseButtonCallBack(GLFWwindow* window, int button, int action, int m);
        // check if currently pressed
        static bool InputCurrent(int key);
        // triggered
        static bool InputTriggered(int key);
        // Gets the mouse position in NDC
        static const glm::vec2& GetMousePosition();
    private:
        // static class
        Input() = delete;

        // checks if a key is valid
        static bool KeyInRange(int key);
    };
}
