/******************************************************************************/
/*
* @file   game.cpp
* @author Aditya Harsh
* @brief  Contains core game startup, run, and exit logic.
*/
/******************************************************************************/

#include "game.h"
#include "utility.h"
#include "core.h"
#include "graphics.h"
#include "gametime.h"
#include "threadmanager.h"
#include "jobmanager.h"
#include "gamemanager.h"
#include "collisionmanager.h"
#include "postprocess.h"

namespace mifty
{
    /**
     * @brief Boot up the game
     * 
     */
    void Game::Boot() noexcept
    {
        // top-level exception handling
        try
        {
            Init();
            Run();
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << '\n';
        }
        catch (...)
        {
            std::cout << "Something threw an exception!\n";
        }
        try
        {
            ShutDown();
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << '\n';
        }
        catch (...)
        {
            std::cout << "Shut down failed!\n";
        }
    }

    /**
     * @brief Initializes all resources
     * 
     * @param options Handles passed in options
     */
    void Game::Init()
    {
        // initialize core systems
        Core::InitCore();

        // initialize threads
        ThreadManager::InitThreads();

        // initialize the graphics
        Graphics::InitGraphics();

        // initialize the game manager
        GameManager::Init();

        // initialize the post processor
        PostProcess::Init(Graphics::GetWindowSize());
    }

    /**
     * @brief Holds the main loop of the game
     * 
     */
    void Game::Run()
    {
        while (1)
        {
            // begin drawing to the post processor
            PostProcess::Begin();

            // calculate how much time has passed since the last frame
            GameTime::UpdateDeltaTime();

            GameTime::FrameLimit();
            
            // poll any window events
            Graphics::PollEvents();

            // Update the game manager
            GameManager::Update();

            // Finish any remaining tasks
            JobManager::RunTasks();

            // update any effects
            PostProcess::Update();

            // render the frame
            Graphics::Render();

            // TODO: Have some handling of exiting
            if (Graphics::ShouldClose())
                break;
        }
    }

    /**
     * @brief Exit and free all resources
     * 
     */
    void Game::ShutDown()
    {
        // Shut down the post processor
        PostProcess::ShutDown();
        
        // Shut down collisions
        CollisionManager::Clear();

        // Shuts off the job manager
        JobManager::ShutDown();

        // Join all of the threads
        ThreadManager::Join();

        // Shut down the game manager
        GameManager::ShutDown();

        // Shut down graphics
        Graphics::ShutDown();
    }
}