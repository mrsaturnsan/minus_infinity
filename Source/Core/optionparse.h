/******************************************************************************/
/*
* @file   optionparse.h
* @author Aditya Harsh
* @brief  Parse provided options.
*/
/******************************************************************************/

#pragma once

namespace mifty
{
    class OptionParse
    {
    public:
        static void Parse(int argc, char** argv) noexcept;
        static bool LockMouse() noexcept;
        static int GetObjects() noexcept;
    private:
        OptionParse() = delete;
    };
    
}
